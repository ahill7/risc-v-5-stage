module fowarding_unit(
    input[31:0] Cycles,
   input[4:0] MEM_writeReg, WB_writeReg, EX_rs1, EX_rs2,
   input MEM_WriteF, WB_WriteFf,
   output reg[1:0] rs1_forward, rs2_forward
);

    //Frowading to rs1 
    always@(MEM_WriteF or WB_WriteFf or MEM_writeReg or WB_writeReg or EX_rs1 or EX_rs2 )begin
        if(MEM_WriteF && (MEM_writeReg != 0) && EX_rs1 == MEM_writeReg)begin
         
            rs1_forward <= 2'b10;
        end
        else if(WB_WriteFf && (WB_writeReg !=0) && (WB_writeReg == EX_rs1))begin
        
            rs1_forward <= 2'b01; 
        end
        else begin
         
            rs1_forward <= 2'b00;
         end

    end


    //Fowarding to rs2
    always@(MEM_WriteF or WB_WriteFf  or MEM_writeReg or WB_writeReg  or EX_rs1 or EX_rs2 )begin
        if(MEM_WriteF && (MEM_writeReg != 0) && EX_rs2 == MEM_writeReg)begin
            
            rs2_forward <= 2'b10;
        end
        else if(WB_WriteFf && (WB_writeReg !=0) && (WB_writeReg == EX_rs2))begin
        
            rs2_forward <= 2'b01; 
        end
        else begin
         
            rs2_forward <= 2'b00;
         end

    end


endmodule
