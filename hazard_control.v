/*
* hazard_control.v
*/

module hazard_control(
	input[4:0] curr_rs1, curr_rs2, EX_rd, MEM_rd,
	input[6:0] opcode,
	input inRegWriteF,
	output reg regWriteF, stall
);

	/*Types of instructions*/
	parameter[6:0] Arithmetic   = 7'b0110011,
		   		   Arithmetic_I = 7'b0010011,
			       SW     	    = 7'b0100011,
				   LW		    = 7'b0000011,
				   Branches		= 7'b1100011,
				   JALR   		= 7'b1100111,
				   JAL			= 7'b1101111,
				   AUIPC		= 7'b0010111,
				   LUI			= 7'b0110111;	   

always@(curr_rs1, curr_rs2, EX_rd, MEM_rd, regWriteF, stall) begin
	case(opcode)
		Arithmetic: begin
			if((curr_rs2 == EX_rd) || (curr_rs1 == EX_rd))begin
			//$display("\n\n##### EXE Hazard #######\n\n");
				stall= 1;
				regWriteF = 0;
			end
			else if((curr_rs2 == MEM_rd) | (curr_rs1 == MEM_rd)) begin
			//$display("\n\n##### MEM Hazard #######\n\n");
				stall= 1;
				regWriteF = 0;
			end
			else begin
				stall= 0;
				regWriteF <= inRegWriteF;
			end
		end

	endcase
end


endmodule
