/*
* RISC_V_CPU_TB.v
* Andrew Hill
*/

module RISC_V_CPU_TB;
	reg clock;
	wire[31:0] result;
	integer i=0, num_instructions=29, j=0;

	RISC_V_CPU uut(
		.clock(clock),
		.result(result)
	);



	task print_value; 
		begin
			$display("\nRunning instruction: %d", uut.to_PC);
			#10;/*FETCH*/
			$display("Fetch: incr_PC(%d) | instruction(%b) | from_PC(%d) | writeData(%d)", uut.incr_PC, uut.instruction, uut.from_PC, uut.writeData);
			#10/*Decode*/	
			$display("Decode: rs1(%d) | rs2(%d) | B_immeditae(%b) | jump_PC(%b) | writeData(%d)",  uut.EX_Control1.operand_B_to_ALU, uut.EX_Control1.operand_A_to_ALU, uut.ID_Control1.B_immediate, uut.jump_PC, uut.writeData);
		
			#10;/*EXECUTE*/
 			$display("Execute: rs1(%d) | rs2(%d) = ALU_Result(%d) | writeData(%d)",  uut.EX_Control1.operand_B_to_ALU, uut.EX_Control1.operand_A_to_ALU, uut.ALU_Result,uut.writeData);

			#10 /*Memory*/
			$display("Memory: writeData(%d) | address(%d) | writeF(%b)", uut.writeData, uut.extended2, uut.memWriteF);

			#10 /*Write Back*/
			$display("Write Back: to_PC(%d) | incr_PC(%d) | writeData(%d)\n",  uut.new_PC, uut.incr_PC, uut.writeData);
		end

	endtask


	always begin
		#5 clock = !clock;
	//	uut.to_PC = uut.new_PC;
	end
	initial begin
		uut.reset = 1;
		#1;
		uut.reset = 0;
	end



	initial begin
		$display("\n#################################################################################");
		$display("\t\t\t RISC-V TEST BED");
		$display("#################################################################################\n");
		
	//	$monitor("new_PC(%d)", uut.new_PC);

		clock = 0;
		//uut.to_PC = 0;
		uut.ID_Control1.Register_File1.reg_file[0]=0;
		uut.ID_Control1.Register_File1.reg_file[1]=2;
		uut.ID_Control1.Register_File1.reg_file[2]=4;
		uut.ID_Control1.Register_File1.reg_file[3]=2**32-1;
		
		print_value;
		print_value;
		//$display("reg_file[%d] = %b", 5, uut.ID_Control1.Register_File1.reg_file[5]);
		// print_value;
		// print_value;
		// print_value;
		//print_value;
		//print_value;
		// for(i=0; i<num_instructions; i = i+1)begin	
		// 	print_value;
		// end


		// $display("\n######################### Register Values ################################");

		// for(i=0; i<32; i=i+1) begin
		//  	$display("reg_file[%d] = %b", i[5:0], uut.ID_Control1.Register_File1.reg_file[i]);
		// end
		
		// print_value;
		// print_value;		
		// $display("data_mem[0]  = %b", uut.MEM_Control1.Data_Memory1.DATA_RAM[0]);
		// $display("data_mem[4]  = %b", uut.MEM_Control1.Data_Memory1.DATA_RAM[4]);
		// print_value;
		// print_value;
		// $display("reg_file[30] = %b", uut.ID_Control1.Register_File1.reg_file[30]);
		// $display("reg_file[31] = %b", uut.ID_Control1.Register_File1.reg_file[31]);

	#10 $stop;

	end


endmodule

