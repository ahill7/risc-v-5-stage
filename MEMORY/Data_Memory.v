/*
 * Andrew Hill
 * June 2016
 * CAES LAB Univeristy of Oregon
 * 
 * This Module represents the l1d cache in a RISC-V processor.
 */

module Data_Memory(
    input[31:0] address, writeData,
    input memWriteF, memReadF, clock,
    output[31:0] readData
);

parameter RAM_SIZE = 300;
reg[31:0] DATA_RAM [0:RAM_SIZE*4];



always@(address) begin

	DATA_RAM[address] = memWriteF ? writeData : DATA_RAM[address];

   /* if(memWriteF)begin
    $display("\n\n!!!!!!!!!!!!!!!!!!writeing to memory!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	DATA_RAM[address] = writeData;
    end */

end
assign readData = memReadF ? DATA_RAM[address][31:0] : writeData;

endmodule
