/* 
* Data_Memory_TB.v
* Andrew Hill
*/

module data_mem_testbed;
	reg[31:0] address, writeData;
	reg memWriteF, memReadF, clock;
	wire[31:0] readData;

	Data_Memory uut(
		.address(address),
		.writeData(writeData),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.clock(clock),
		.readData(readData)
	);

	always begin
		#5 clock = !clock;
	end

	initial begin
		$display("###########################################################################################");
		$display("\t\t################Data Mem TEST################");
		$display("###########################################################################################");
		clock=0;
		address=0;
		writeData=69;
		memWriteF=1;
		memReadF=0;
		$display("mem[0]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);
		#10;
		memWriteF=0;
		memReadF=1;
		#10;
		$display("mem[0]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);

		//Address 2
		address=1;
		writeData=9;
		memWriteF=1;
		memReadF=0;
		$display("mem[1]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);
		#10;
		memWriteF=0;
		memReadF=1;
		#10;
		$display("mem[1]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);
		
		//Address 2
		address=2;
		writeData=24;
		memWriteF=1;
		memReadF=0;
		$display("mem[2]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);
		#10;
		memWriteF=0;
		memReadF=1;
		#10;
		$display("mem[2]=(%d) || WF(%b) RF(%b) || WriteData(%d) || ReadData(%d)", uut.DATA_RAM[address], memWriteF, memReadF, writeData, readData);










		#10 $stop;
	end



endmodule
