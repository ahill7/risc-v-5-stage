/*
* MEM_Control.v
* Andrew Hill
*
*
*/


module MEM_Control(
    input[31:0] ALU_Result, outRegB,
    input[31:0] extended2, 
    input[31:0] immediate_value,
    input memWriteF, memReadF, clock, memMUXF, memWriteMUXF,
    output[31:0] readData
);
	wire[31:0] address, memWriteData;

	assign address = (memMUXF) ? immediate_value : extended2;
	assign memWriteData = (memWriteMUXF) ? outRegB : ALU_Result;

	// mux2to1 memMux(
	// 	.gen0(extended2),
	// 	.gen1(immediate_value),
	// 	.sel(memMUXF),
	// 	.gen_out(address)
	// 	);

	Data_Memory Data_Memory1(
		.address(address),
		.writeData(memWriteData),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.clock(clock),
		.readData(readData)
	); 

	


endmodule
