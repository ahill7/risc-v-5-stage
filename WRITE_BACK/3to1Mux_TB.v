/* 
* 3to1Mux_TB.v
* Andrew Hill
*
*/



module Threeto1Mux_TB;
	reg[31:0] genericInput1, genericInput2, genericInput3;
	reg[1:0] genericFlag;
	wire[31:0] genericOutput;	

	Threeto1Mux uut(
		.genericInput1(genericInput1),
		.genericInput2(genericInput2),
		.genericInput3(genericInput3),
		.genericFlag(genericFlag),
		.genericOutput(genericOutput)	
	);
	
	initial begin
		//genericInput1 shouldbe the resutl
		genericInput1=1;
		genericInput2=2;
		genericInput3=3;
		genericFlag=2'b00;
		#10;
		$display("g1(%d), g2(%d), g3(%d), Flag(%b), result=(%d)", genericInput1, genericInput2, genericInput3, genericFlag, genericOutput);
		#10;
		//genericInput2 shouldbe the resutl
		genericFlag=2'b01;
		#10;
		$display("g1(%d), g2(%d), g3(%d), Flag(%b), result=(%d)", genericInput1, genericInput2, genericInput3, genericFlag, genericOutput);
		#10;
		//genericInput3 shouldbe the resutl
		genericFlag=2'b10;
		#10;
		$display("g1(%d), g2(%d), g3(%d), Flag(%b), result=(%d)", genericInput1, genericInput2, genericInput3, genericFlag, genericOutput);



	end




endmodule
