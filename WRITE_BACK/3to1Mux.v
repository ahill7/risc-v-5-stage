/* 
* 3to1Mux.v
* Andrew Hill
*
*/


module Threeto1Mux(
	input[31:0] genericInput1,
    input[31:0]	genericInput2,
    input[31:0]	genericInput3, 
	input[1:0] genericFlag,
	output reg[31:0] genericOutput 
);

	always@(*)begin
		case(genericFlag)
			2'b00: genericOutput = genericInput1;
			2'b01: genericOutput = genericInput2;
			2'b10: genericOutput = genericInput3;
			default: genericOutput = 0;
		endcase
	end


endmodule
