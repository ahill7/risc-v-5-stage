/*
* mux4to1.v
* Andrew Hill
*/

module mux4to1(
	input[31:0] gen0, gen1, gen2, gen3,
	input[1:0] sel,
	output reg[31:0] gen_out
);

	always@(*) begin
		case(sel)
			2'b00: gen_out = gen0;
			2'b01: gen_out = gen1;
			2'b10: gen_out = gen2;
			2'b11: gen_out = gen3;
			default: gen_out = 0;
		endcase

	end




endmodule
