/*
* ALU+ALU_Control_Tb.v
*/

module ALU_ALU_Control_Tb;
	reg[2:0] funct3;
	reg[6:0] funct7;
	reg[1:0] ALU_opF;
	reg[31:0] rs2, rs1, incr_PC, branch;
	reg branchF;
	wire[31:0]ALUResult, branch_PC;
	wire zeroF;
	reg[31:0] random=0;

	ALU_ALU_Control uut(
		.branch(branch),
		.incr_PC(incr_PC),
		.funct3(funct3),
		.funct7(funct7),
		.ALU_opF(ALU_opF),
		.rs2(rs2), 
		.rs1(rs1),
		.ALUResult(ALUResult),
		.branch_PC(branch_PC),
		.zeroF(zeroF)
	);


	parameter[2:0] BEQ  = 3'b000,
				   BNE  = 3'b001,
				   BLT  = 3'b100,
				   BGE  = 3'b101,
				   BLTU = 3'b110,
				   BGEU = 3'b111;



	
	task take_branch;
		begin
				if((zeroF && (funct3 == BEQ || funct3 == BGE || funct3 == BGEU)) || (!zeroF && (funct3 == BNE || funct3 == BLT || funct3 == BLTU)))begin
					$display("######## take branch ########\n\n");
				end
				else begin
					$display("####### don't take branch ########\n\n");
				end

		end
	endtask




	initial begin
		$display("\n###################################################################################");
		$display("\t\t\t\t ALU + ALU Control");
	   	$display("###################################################################################\n");
		
		random[30:0] = $random;

		$display("\n############### BEQ ######################\n\n");
		ALU_opF = 2'b10;
		funct7 = 0100000;
		funct3 = 3'b000; /*BEQ, sub and if(zeroF)*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) + (-)rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;


		$display("\n############### BNE ######################\n\n");
		funct3 = 3'b001; /* BNE, sub to if(!zero)*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) + (-)rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) + (-)rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;



		$display("\n############### BLT ######################\n\n");
		funct3 = 3'b100; /* BLT, slt && !zeroF*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) < rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;



		$display("\n############### BLTU ######################\n\n");
		funct3 = 3'b110; /*BLTU, sltu && !zeroF*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) < rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) < rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;



		$display("\n############### BGE ######################\n\n");
		funct3 = 3'b101; /* BGE, slt && zeroF*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) > rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;




		$display("\n############### BGEU ######################\n\n");
		funct3 = 3'b111; /*BGEU, sltu && zeroF*/
		rs2 = 1;
		rs1 = 0;
		#10;
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 2;
		#10
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10
		rs1 = 1;
		#10
		$display("rs1(%d) > rs2(%d)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;
		#10;
		rs1 = 2**32-1;
		rs2 = random;
		#10;
		$display("rs1(%b) > rs2(%b)", uut.rs1, uut.rs2);
		$display("ALUResult(%d) | zeroF(%b)", uut.ALUResult, uut.zeroF);	
		take_branch;




		$stop;
                                                                                                                                                                                                             
	end                                                                                                   





endmodule
