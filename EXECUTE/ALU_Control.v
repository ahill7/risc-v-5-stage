/* 
* ALU_Control.v
* Andrew Hill
*
*/


module ALU_Control(
	input[2:0] funct3,
	input[6:0] funct7_in,
	input[1:0] ALU_opF,
	output reg[2:0] ALUControlOpcode,
	output reg[6:0] funct7_out
);

	always@(*)begin
		case(ALU_opF)
			2'b00: begin 
				ALUControlOpcode = funct3;
				funct7_out = funct7_in;
			end
			2'b01: begin
				   	ALUControlOpcode = 000;
					funct7_out = funct7_in;
			end
			2'b10: begin /*Used for branches*/
				case(funct3)
					3'b000: ALUControlOpcode = 000; /*BEQ, subtraction && if zeroF*/
					3'b001: ALUControlOpcode = 000; /*Bne, subtraction && if !zeroF*/
					3'b100: ALUControlOpcode = 010; /*BLT, slt && !zeroF*/
					3'b101: ALUControlOpcode = 010; /*BGE, slt && zeroF*/
					3'b110: ALUControlOpcode = 011; /*BLTU, sltu && !zeroF*/
					3'b111: ALUControlOpcode = 011; /*BGEU, sltu && zeroF*/
				endcase
				funct7_out = 0100000;
			end
			/*Implement rest of the ALU control here.*/
			default:begin
				ALUControlOpcode = funct3;
				funct7_out       = funct7_in;
			end
		endcase
	end

endmodule
