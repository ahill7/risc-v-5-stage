/* 
* ALU Testbed
* Andrew Hill
* CAES Lab, Univerity of Oregon
* June 2016
*
*/

module ALU_testbed;
	reg[31:0] outRegA;
	reg[31:0] outRegB;
	reg[6:0] ALUControlOpcode;
	wire[31:0] ALUResult;
	wire zeroF;

	ALU uut(
		.outRegA(outRegA),
		.outRegB(outRegB),
		.ALUControlOpcode(ALUControlOpcode),
		.ALUResult(ALUResult),
		.zeroF(zeroF)
	);


	initial begin
		$display("\n-------------------------------------------------------------");
		$display("\t\t\tALU TESTBED");
		$display("-------------------------------------------------------------");
		outRegA = 0;
		outRegB = 0;
		ALUControlOpcode = 0;





		outRegA=1;
		outRegB=1;
		ALUControlOpcode = 3'b00_0;
		#10;
		$display("\nADD TESTs\n");
		$display("outRegA(%d) | outRegB(%d) | ALUControlOpcode(%b) | ALUResult(%d)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		//ADD test2
		outRegA = 0;
		outRegB = 100;
		#10;
		$display("outRegA(%d) | outRegB(%d) | ALUControlOpcode(%b) | ALUResult(%d)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		
		//SLL Tests1
		$display("\nSLL TESTs");
		ALUControlOpcode=3'b00_1;
		outRegA = 2**30-1;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		//SLL Tests2
		outRegB = 2;
		#10
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 2**20;
		//SLL Tests3
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

		$display("\nSLT TESTs"); 
		//SLT signged tests
		ALUControlOpcode=3'b01_0;
		outRegA = 0;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 100;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 101;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**32-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

		//Unsigned SLL TESTs.
		$display("\nUNSIGNEd SLt");
		ALUControlOpcode = 3'b01_1;
		outRegA = 0;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 100;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 101;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**32-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);


		//XOR test
		$display("\nXOr TESTs");
		ALUControlOpcode = 3'b10_0;
		outRegA = 255;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 2**32-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 0;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**32-1;
		outRegB = 2**32-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

		//SRL test1
		$display("\nSRL TESTs");
		ALUControlOpcode = 3'b10_1;
		outRegA = 2**32-1;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 2;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		//outRegA = 64;
		outRegB = 16;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA =  2**17-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

		//OR test1
		$display("\nOR TESTs");
		ALUControlOpcode = 3'b11_0;
		outRegA = 0;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 127;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegB = 127;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**32-1;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**31;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

		//AND test1
		$display("\nAND TESTs");
		ALUControlOpcode = 3'b11_1;
		outRegA = 0;
		outRegB = 0;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 127;
		outRegB = 127;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**32-1;
		outRegB = 2**28 - 2**20-50;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);
		outRegA = 2**31;
		#10;
		$display("outRegA(%b) | outRegB(%b) | ALUControlOpcode(%b) | ALUResult(%b)",outRegA, outRegB, ALUControlOpcode, ALUResult);

	end

	
endmodule
