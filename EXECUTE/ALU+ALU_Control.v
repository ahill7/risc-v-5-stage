/*
* Andrw Hill
* CAES LAB, University of Oregon
* July 2016
*/


module ALU_ALU_Control(
	input[2:0] funct3,
	input[1:0] ALU_opF,
	input[31:0] rs2, rs1, incr_PC, branch,
	input[6:0] funct7,
	output signed[31:0]ALUResult, branch_PC,
	output zeroF


);
	wire[2:0] ALUControlOpcode;


	ALU_Control ALU_Control1(
		.funct3(funct3),
		.ALU_opF(ALU_opF),
		.ALUControlOpcode(ALUControlOpcode)
	);

	
	ALU ALU1(
		.rs2(rs2),
		.funct3(funct3),
		.branch(branch),
		.incr_PC(incr_PC),
		.rs1(rs1),
		.ALUControlOpcode(ALUControlOpcode),
		.funct7(funct7),
		.ALUResult(ALUResult),
		.zeroF(zeroF),
		.branch_PC(branch_PC)
	);



endmodule
