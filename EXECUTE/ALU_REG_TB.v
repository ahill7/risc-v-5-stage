/* 
* AL_REG_TB.v
* Andew Hill
* CAES Lab
* Univeristy of Oregon
* June 2016
*/

module ALU_REG_TB;//Register File.
    reg regWriteF; 
	reg clock;
    reg[4:0] inRegA;
   	reg[4:0] inRegB; 
	reg[4:0] writeReg;
    reg[31:0] writeData;
    wire[31:0] outRegA; 
	wire[31:0] outRegB;
	//ALU	
	reg[6:0] ALUControlOpcode;
	wire[31:0]ALUResult;
	wire zeroF;


	ALU_REG uut(
		.regWriteF(regWriteF),
		.clock(clock),
		.inRegA(inRegA),
		.inRegB(inRegB),
		.writeReg(writeReg),
		.writeData(writeData),
		.outRegA(outRegA),
		.outRegB(outRegB),
		.ALUControlOpcode(ALUControlOpcode),
		.ALUResult(ALUResult),
		.zeroF(zeroF)
	);

	always begin
		#5 clock = !clock;
	end

	initial begin
		$display("#####################################################################################################################################");
		$display("\t\t\t\t\t\tALU_REG TESTBED");
		$display("#####################################################################################################################################");
		clock=1;
		inRegA = 0;
		inRegB = 0;
		writeReg = 0;
		uut.RF_1.reg_file[0]=0;	
		uut.RF_1.reg_file[1]=2**32-1;	
		uut.RF_1.reg_file[2]=7269;	
		uut.RF_1.reg_file[3]=8;	
		uut.RF_1.reg_file[4]=4;	
		uut.RF_1.reg_file[5]=12121;	
		uut.RF_1.reg_file[6]=1;
		

		$display("\nADD");
		//ADD 000
		regWriteF = 0;
		inRegA = 0;
		inRegB = 3;
		#10;
		ALUControlOpcode = 3'b00_0;
		writeReg = 10;
		regWriteF = 1;
		#20;
		$display("inRegA(%d) = (%d) | inRegB(%d) = (%d) | writeReg(%d) | op(%b) | output(%b)", inRegA, outRegA, inRegB, outRegB, writeReg, ALUControlOpcode, ALUResult);
		//ADD test2
		regWriteF = 0;
		inRegA = 4;
		#10;
		regWriteF = 1;
		#20;
		$display("(%d) + (%d) = (%d)", outRegA, outRegB, ALUResult);

		$display("\nShift Left");
		//SLL 001
		regWriteF=0;
		inRegA=1;
		inRegB=6;
		ALUControlOpcode=3'b00_1;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) left by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SLL 001 TEST 2
		regWriteF=0;
		inRegB=4;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) left by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SLL 001 TEST 2
		regWriteF=0;
		inRegB=3;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) left by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);


		$display("\nSLT signed");
		//SLT 010 TEST1
		regWriteF=0;
		ALUControlOpcode = 3'b01_0;
		inRegA=5;
		inRegB=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) = ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST2
		regWriteF=0;
		inRegA=0;
		inRegB=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) = ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST3
		regWriteF=0;
		inRegA=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) = ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST4
		regWriteF=0;
		inRegA=1;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) = ALUResult(%d)", outRegA, outRegB, ALUResult);


		$display("\nSLT unsigned");
		//SLTU 010 TEST1
		regWriteF=0;
		ALUControlOpcode = 3'b01_1;
		inRegA=5;
		inRegB=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) || ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLTU 010 TEST2
		regWriteF=0;
		inRegA=0;
		inRegB=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) || ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLTU 010 TEST3
		regWriteF=0;
		inRegA=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) || ALUResult(%d)", outRegA, outRegB, ALUResult);
		//SLTU 010 TEST4
		regWriteF=0;
		inRegA=1;
		#10;
		regWriteF=1;
		#20;
		$display("(%d) < (%d) || ALUResult(%d)", outRegA, outRegB, ALUResult);

		$display("\nXOR");
		//SLT 010 TEST1
		regWriteF=0;
		ALUControlOpcode = 3'b10_0;
		inRegA=5;
		inRegB=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST2
		regWriteF=0;
		inRegA=0;
		inRegB=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST3
		regWriteF=0;
		inRegA=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SLT 010 TEST4
		regWriteF=0;
		inRegA=1;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
	

		$display("\nShift right");
		//SRL 001
		regWriteF=0;
		inRegA=1;
		inRegB=6;
		ALUControlOpcode=3'b10_1;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) right by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SRL 001 TEST 2
		regWriteF=0;
		inRegB=4;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) right by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//SRL 001 TEST 2
		regWriteF=0;
		inRegB=3;
		#10;
		regWriteF=1;
		#20;
		$display("Shifting(%b) right by (%d) = ALUResult(%b)", outRegA, outRegB, ALUResult);

		$display("\nOR");
		//or 010 TEST1
		regWriteF=0;
		ALUControlOpcode = 3'b11_0;
		inRegA=5;
		inRegB=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//or 010 TEST2
		regWriteF=0;
		inRegA=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//or 010 TEST3
		regWriteF=0;
		inRegA=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//or 010 TEST4
		regWriteF=0;
		inRegA=1;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		

		$display("\nAND");
		//and 010 TEST1
		regWriteF=0;
		ALUControlOpcode = 3'b11_1;
		inRegA=5;
		inRegB=0;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//and 010 TEST2
		regWriteF=0;
		inRegA=0;
		inRegB=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//and 010 TEST3
		regWriteF=0;
		inRegA=5;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);
		//and 010 TEST4
		regWriteF=0;
		inRegA=1;
		#10;
		regWriteF=1;
		#20;
		$display("(%b) ^ (%b) = ALUResult(%b)", outRegA, outRegB, ALUResult);

	#10 $stop;
	end	
endmodule
