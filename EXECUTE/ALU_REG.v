/*
* ALU_REG.v
* Andrew Hill
* CAES Lab
* University of Oregon
* June 2016
*
*/
module ALU_REG(
	//Register File.
    input regWriteF, clock,
    input[4:0] inRegA, inRegB, writeReg,
    input[31:0] writeData,
    output[31:0] outRegA, outRegB,
    //ALU	i
    input[6:0] ALUControlOpcode,
    output[31:0]ALUResult,
    output zeroF
);

reg[31:0] finalA, finalB;

Register_File RF_1(
    .regWriteF(regWriteF),
	.clock(clock),
    .inRegA(inRegA),
	.inRegB(inRegB),
	.writeReg(writeReg),
    .writeData(writeData),
    .outRegA(outRegA),
	.outRegB(outRegB)
);

ALU ALU_1(
	.outRegA(outRegA),
	.outRegB(outRegB),
	.ALUControlOpcode(ALUControlOpcode),
	.ALUResult(ALUResult),
	.zeroF(zeroF)
);


endmodule
