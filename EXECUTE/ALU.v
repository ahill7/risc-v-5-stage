/*
* Andrew Hill
* CAES Lab, Univerity of Oregon
* June 2016
* 
* TODO: Redo Test Bed.
*
*
*
*/ 


module ALU(
	input[31:0] rs2, rs1, branch, incr_PC,
	input[2:0] ALUControlOpcode, funct3,
	input[6:0] funct7,
	output reg [31:0]ALUResult, branch_PC,
	output reg zeroF
);


	parameter[2:0] BEQ  = 3'b000,
				   BNE  = 3'b001,
				   BLT  = 3'b100,
				   BGE  = 3'b101,
				   BLTU = 3'b110,
				   BGEU = 3'b111;




	wire signed[31:0] rs2_signed, rs1_signed, twos_comp;
	wire [4:0] shamt;
	/*TODO:Find out what the shamt should be.*/
	assign shamt = rs2[4:0]; /*Dont think this is right, check*/
	assign rs2_signed = rs2;
	assign rs1_signed = rs1;
	assign twos_comp = (~rs2_signed) + 1;
	always@(*)begin

		case(ALUControlOpcode)

			3'b00_0: begin
			$display("\nf7(%b) | f3(%b)\n", funct7, funct3);
					if(funct7 == 7'b0100000)begin
						
						ALUResult = rs1_signed -rs2_signed; /* TODO: replace with rs1_signed - rs2_signed*/ 
				   	end
				   	else begin
				   		
				   		ALUResult = rs1_signed + rs2_signed; /*Add, Addi*/
					end
			end
			3'b001: ALUResult = rs1 << shamt;/*SLLI, SLL*/
			3'b010: ALUResult = rs1_signed < rs2_signed;/*SLTI, SLT*/
			3'b011: ALUResult = rs1 < rs2;/*SLTIU, SLTU*/
			3'b100: ALUResult = rs1 ^ rs2;/*XORI, XOR*/
			3'b101: begin
				if(funct7) ALUResult = rs1_signed >>> shamt; /* Shift right arithmatic*/
				else ALUResult = rs1_signed >> shamt;/*SRLI, SRL*/
			end
			3'b110: ALUResult = rs1 | rs2;/*ORI, OR*/
			3'b111: ALUResult = rs1 & rs2;/*ANDI, AND*/
			default: ALUResult = 0;

		endcase

		if(!ALUResult) zeroF = 1;
		else zeroF=0;


	// 	/* Make the brach_PC a seperate module*/
	// 	if((zeroF && (funct3 == BEQ || funct3 == BGE || funct3 == BGEU)) || (!zeroF && (funct3 == BNE || funct3 == BLT || funct3 == BLTU)))begin
	// 		branch_PC = branch;
	// //		$display("######## take branch ########\n\n");
	// 	end
	// 	else begin
	// 		branch_PC = incr_PC;
	// 		//$display("####### don't take branch ########\n\n");
	// 	end
		

	end
endmodule
