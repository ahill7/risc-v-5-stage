/* 
* control_unit.v
* Andrew Hill
*
*/

module control_unit(
	input[6:0] control_opcode,
	input[6:0] func7, /*instruction[31:25]*/
	output reg[1:0] PC_SelectF,
	output reg regWriteF, memMUXF, memWriteMUXF,
	output reg[1:0] regDestF,
	output reg[2:0] ALU_a_MUXF,
	output reg[1:0] ALU_b_MUXF,
	output reg[1:0] ALU_opF,
	output reg memWriteF,
	output reg memReadF, 
	output reg[1:0] writeback_selectF
	//Rest of the flags are to be set here.

);


	/*Types of instructions*/
	parameter[6:0] Arithmetic   = 7'b0110011,
		   		   Arithmetic_I = 7'b0010011,
			       SW     	    = 7'b0100011,
				   LW		    = 7'b0000011,
				   Branches		= 7'b1100011,
				   JALR   		= 7'b1100111,
				   JAL			= 7'b1101111,
				   AUIPC		= 7'b0010111,
				   LUI			= 7'b0110111;	   



	always@(control_opcode, func7)begin
			case(control_opcode)
					Arithmetic: begin
						PC_SelectF        = 2'b00;
						regWriteF         = 1;
						regDestF 		  = 0;
						ALU_a_MUXF 		  = 3'b000; /*outRegA*/
						ALU_b_MUXF 		  = 2'b00; /*outRegB*/
						ALU_opF 		  = 2'b00; /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 0;
						writeback_selectF = 0; /*ALU_Result*/
						memMUXF  		  = 0;
						memWriteMUXF 	  = 0;
					end
					Arithmetic_I:begin
						PC_SelectF		  = 2'b00;
						regWriteF 		  = 1;
						regDestF 		  = 0;
						ALU_a_MUXF 		  = 3'b001; /*immediate value*/
						ALU_b_MUXF 		  = 2'b00; /*outRegB*/
						ALU_opF 		  = 2'b00; /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 0;
						writeback_selectF = 0; /*ALU_Result*/
						memMUXF 		  = 0;
						memWriteMUXF 	  = 0;
					end	
					LUI: begin
						PC_SelectF		  = 2'b00;
						regWriteF 		  = 1;
						regDestF 		  = 0;
						ALU_a_MUXF 		  = 3'b011;   /*from zero*/
						ALU_b_MUXF 		  = 2'b11;     /*outRegB*/
						ALU_opF 		  = 2'b01; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 0;
						writeback_selectF = 0; /*ALU_Result*/
						memMUXF		  	  = 0;
						memWriteMUXF	  = 0;
					end
					AUIPC: begin
						PC_SelectF		  = 2'b00;
						regWriteF 		  = 1;
						regDestF 		  = 0;
						ALU_a_MUXF 		  = 3'b011;   /*from zero*/
						ALU_b_MUXF 		  = 2'b01;     /*outRegB*/
						ALU_opF 		  = 2'b01; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 0;
						writeback_selectF = 0; /*ALU_Result*/
						memMUXF 		  = 0;
						memWriteMUXF 	  = 0;
					end
					SW: begin
						PC_SelectF		  = 2'b00;
						regWriteF 		  = 0;
						regDestF 		  = 0;
						ALU_a_MUXF 		  = 3'b100;   /*extended2*/
						ALU_b_MUXF		  = 2'b00;     /*outRegB*/
						ALU_opF 		  = 2'b01; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 1;
						memReadF 		  = 0;
						writeback_selectF = 0; /*ALU_Result*/
						memMUXF 		  = 0;
						memWriteMUXF	  = 1;
					end
					LW: begin
						PC_SelectF		  = 2'b00;
						regWriteF 		  = 1;
						regDestF 		  = 0;
						ALU_a_MUXF		  = 3'b100;   /*extended 2*/
						ALU_b_MUXF 		  = 2'b00;     /*outRegB*/
						ALU_opF 		  = 2'b01; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 1;
						writeback_selectF = 2'b01; /*ALU_Result*/
						memMUXF 		  = 1;
						memWriteMUXF 	  = 1;
					end
					JAL: begin
						PC_SelectF 		  = 2'b10;
						regWriteF 		  = 1'b1;
						regDestF 		  = 1'b0;
						ALU_a_MUXF 		  = 3'b010;   /*from zero aka jump PC*/
						ALU_b_MUXF 		  = 2'b01;     /*outRegB*/
						ALU_opF 		  = 2'b01; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 0;
						memReadF 		  = 0;
						writeback_selectF = 2'b10; /*ALU_Result*/
						memMUXF		      = 0;
						memWriteMUXF	  = 0;
					end
					JALR: begin
						PC_SelectF 		  = 2'b11; /* CHooses JALR_Result to be next PC */
						regWriteF 		  = 1'b1;
						regDestF 		  = 1'b0;
						ALU_a_MUXF 		  = 3'b001; /* immediate value */  
						ALU_b_MUXF 		  = 2'b00;  /* outRegB */   
						ALU_opF 		  = 2'b01; 	/*operandA + operandB*/   
						memWriteF 		  = 1'b0;
						memReadF 		  = 1'b0;
						writeback_selectF = 2'b10; /* Writes incr_PC to rd*/
						memMUXF		      = 1'b0;
						memWriteMUXF	  = 1'b0;
					end	
					Branches: begin
						PC_SelectF 		  = 2'b01; /*Setting next PC to Branch result*/
						regWriteF 		  = 1'b0;
						regDestF 		  = 1'b0;
						ALU_a_MUXF 		  = 3'b000;   /*outRegA*/
						ALU_b_MUXF 		  = 2'b00;     /*outRegB*/
						ALU_opF 		  = 2'b10; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 1'b0;
						memReadF 		  = 1'b0;
						writeback_selectF = 2'b01; /*ALU_Result*/
						memMUXF		      = 1'b0;
						memWriteMUXF	  = 1'b0;
					end
					default: begin
						PC_SelectF 		  = 2'b00; /*Setting next PC to Branch result*/
						regWriteF 		  = 1'b0;
						regDestF 		  = 1'b0;
						ALU_a_MUXF 		  = 3'b000;   /*outRegA*/
						ALU_b_MUXF 		  = 2'b00;     /*outRegB*/
						ALU_opF 		  = 2'b00; 	   /*Use 3 bit function encoding*/
						memWriteF 		  = 1'b0;
						memReadF 		  = 1'b0;
						writeback_selectF = 2'b00; /*ALU_Result*/
						memMUXF		      = 1'b0;
						memWriteMUXF	  = 1'b0;
					end
			endcase	
		end

endmodule
