/*
* RISC_V_PIPELINE_CPU.v
*/


module RISC_V_PIPELINE_CPU(
input clock,
output[31:0] result

);


	/*Fetch registers and wires*/ 
		reg[31:0] to_PC = 0;
		reg reset;
		wire[31:0] incr_PC, instruction, from_PC;


	/*Control registers and wires*/
		wire[6:0] control_opcode = instruction[6:0];
		wire[6:0] func7 = instruction [31:25];
		wire[1:0] PC_SelectF; 
		wire regWriteF;
		wire[1:0] regDestF;
		wire[2:0] ALU_a_MUXF;
		wire[1:0] ALU_b_MUXF;
		wire[1:0] ALU_opF;
		wire memWriteF, memReadF;
		wire[1:0] writeback_selectF;
		wire memMUXF, memWriteMUXF;


	/*Decode registers and wires*/
		wire[31:0] writeData;
		wire[31:0] extended2, immediate_value, branch, jump_PC, outRegA, outRegB, from_zero, from_zero_large;
		wire[31:0] temp_PC;


	/*Execute registers and wires*/
		wire[31:0] ALU_Result, operandA;
		wire[31:0] branch_PC;


	/*Memory registers and Wires*/
	    wire[31:0] address;
		wire[31:0] readData, memWriteData;
		

	/*Write Back registers and wires */
		wire[31:0] new_PC;
		wire[4:0] rd;
		wire WB_regWriteF;


	IF_Control IF_Control1(
		//Inputs
		.to_PC(to_PC),
		.clock(clock),
		.reset(reset),
		//output
		.incr_PC(incr_PC),
		.instruction(instruction),
		.from_PC(from_PC)
	);


	control_unit CU1(
		.control_opcode(control_opcode),
		.func7(func7),
		.memWriteMUXF(memWriteMUXF),
		.PC_SelectF(PC_SelectF),
		.memMUXF(memMUXF),
		.regWriteF(regWriteF),
		.regDestF(regDestF),
		.ALU_a_MUXF(ALU_a_MUXF),
		.ALU_b_MUXF(ALU_b_MUXF),
		.ALU_opF(ALU_opF),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.writeback_selectF(writeback_selectF)
	);



	ID_Control ID_Control1(
		//input
		.instruction(instruction),
		.from_PC(from_PC),
		.rd(rd),
		.writeData(writeData),
		.regWriteF(WB_regWriteF),
		.clock(clock),
		.regDestF(regDestF),
		//output
		.extended2(extended2),
		.immediate_value(immediate_value),
		.branch(branch),
		.jump_PC(jump_PC),
		.outRegA(outRegA),
		.outRegB(outRegB),
		.from_zero(from_zero),
		.from_zero_large(from_zero_large),
		.PC(temp_PC)
	);



	EX_Control EX_Control1(
		.PC(temp_PC),
		.extended2(extended2),
		.immediate_value(immediate_value),
		.branch(branch),
		.incr_PC(incr_PC),
		.jump_PC(jump_PC), /*Can delete this */
		.outRegA(outRegA),
		.outRegB(outRegB),
		.from_zero(jump_PC),/* This is the jump pc, rename this */
		.from_zero_large(from_zero_large),
		.ALU_a_MUXF(ALU_a_MUXF),
		.ALU_opF(ALU_opF),
		.funct3(instruction[14:12]),
		.funct7(instruction[31:25]), /* Double check this is proper index */
		.ALU_b_MUXF(ALU_b_MUXF),
		.zeroF(zeroF),
		.ALU_Result(ALU_Result),
		.operandA(operandA),
		.branch_PC(branch_PC)
	);
	

	/*
	mux2to1 memMux(
		.gen0(extended2),
		.gen1(immediate_value),
		.sel(memMUXF),
		.gen_out(address)
	); */

	assign memWriteData = (memWriteMUXF) ? ALU_Result : outRegB;

	/*
	mux2to1 memWriteDataMux(
		.gen0(ALU_Result),
		.gen1(outRegB),
		.sel(memWriteMUXF),
		.gen_out(memWriteData)
	);*/


	MEM_Control MEM_Control1(
		//.address(address),			
		.writeData(memWriteData), /*writing rs2(src) to memory*/
		.extended2(extended2),
		.immediate_value(immediate_value),
		.memMUXF(memMUXF),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.readData(readData),
		.clock(clock)
	);



	WB_Control WB_Control1(
		.ALU_Result(ALU_Result),
		.readData(readData),
		.incr_PC(incr_PC),
		.regWriteF(regWriteF),
		.writeReg(instruction[11:7]),
		.curr_PC(to_PC),
		.branch_PC(branch_PC),
		.jump(jump_PC),
		.memToRegF(writeback_selectF),
		.PC_SelectF(PC_SelectF),
		.writeData(writeData),
		.to_PC(new_PC),
		.WB_regWriteF(WB_regWriteF),
		.rd(rd)
	);
	
	always begin
	#50	to_PC = new_PC;
	end 


endmodule

