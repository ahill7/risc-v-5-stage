/* 
* mux5to1.v
* Andre Hill
*/


module mux5to1(
	input[31:0] gen1, gen2, gen3, gen4, gen5,
	input[2:0] sel,
	output reg[31:0] gen_out 
);

	always@(*) begin
		case(sel)
			3'b000: gen_out = gen1;
			3'b001: gen_out = gen2;
			3'b010: gen_out = gen3;
			3'b011: gen_out = gen4;
			3'b100: gen_out = gen5;
			default: gen_out= 0;
		endcase
	end


endmodule
