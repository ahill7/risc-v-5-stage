/*
* mux2to1.v
*/

module mux2to1(
	input[31:0] gen0,
	input[31:0] gen1,
	input sel,
	output reg[31:0] gen_out
);

always@(*) begin
	case(sel)
		1'b0: gen_out = gen0;
		1'b1: gen_out = gen1;
		default: gen_out = 0;
	endcase

end
endmodule
