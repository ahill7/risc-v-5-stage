module PC_Adder_testbed;
	reg[31:0] fromPC;
	wire[31:0] incrPC;
	integer i=0;
	PC_adder  uut(
		.fromPC(fromPC),
		.incrPC(incrPC)	
	);


	initial begin

		$display("##################################################################################################");
		$display("\t\t#######\t\tPC_ADDER_TESTBED\t\t########");
		$display("##################################################################################################");


		fromPC=0;
		#10
		$display("fromPC(%d) || incrPC(%d)", fromPC, incrPC);
		fromPC=incrPC;
		#10
		$display("fromPC(%d) || incrPC(%d)", fromPC, incrPC);
		for(i=0; i<20; i=i+1)begin
			fromPC = incrPC;
			#10
			$display("fromPC(%d) || incrPC(%d)", fromPC, incrPC);
		end

		#10 $stop;
	end


endmodule
