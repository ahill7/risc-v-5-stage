module ProgramCounter(
	input[31:0] to_PC,
	input reset, clock,
	output reg[31:0] from_PC
);

	reg doneOnce =1'b0;

	always@(to_PC) begin	
		if(reset) begin
			from_PC = 0;
		end
		else from_PC = to_PC;
	end


endmodule
