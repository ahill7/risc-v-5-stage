/*
* PC_Adder.v
* Andrew Hill
*
*/



module PC_Adder(
	input[31:0] from_PC,
	output reg[31:0] incr_PC 
);


	always@(*)begin

		incr_PC= from_PC+ 4;


	end

endmodule



