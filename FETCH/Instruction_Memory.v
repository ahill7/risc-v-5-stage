/*
 * Andrew Hill
 * CAES LAB
 * Univeristy of Oregon
 * June 2016
 *
 * l1i cache module.
 *
 */

module Instruction_Memory(
    input[31:0] from_PC,
    input clock, reset,
    output reg[31:0] instruction 
   
);

    parameter numberInstructions=100;
    reg[31:0] numberInstructionsLoaded = 0;
    reg[31:0] INSTRUCTION_RAM [0:numberInstructions*4];
    
    always@(from_PC) begin
		instruction <= INSTRUCTION_RAM[from_PC];
    end
	// always@(reset) begin
 // 	 	//INSTRUCTION_RAM[0]   <= 32'b0000000_00010_00001_000_00100_0110011;   /*funct7_rs2_rs1_funct3_rd_opCode | reg_file[3] = reg_file[1] + reg_file[2] => 6 = 4 + 2*/
	// 	// INSTRUCTION_RAM[4]   <= 32'b0_000000110_0_00000000_01100_1101111; /* jump */
	// 	// INSTRUCTION_RAM[16]  <= 32'b0_0000000110_0_00000000_01101_1101111; /* jump */
	// 	INSTRUCTION_RAM[4]   <= 32'b000000001001_00001_000_00101_0010011;    /*addi reg[5] = rs2 + 9*/
	// 	INSTRUCTION_RAM[8]   <= 32'b0000000_00001_00010_001_00110_0110011;   /*sll reg[6] = rs2 << rs1 => 00100 << 10 = 10000*/
	//     INSTRUCTION_RAM[12]  <= 32'b0000000_00001_00010_010_00111_0110011;   /*SLT reg[7] = reg[2] < reg[1]*/
	//     INSTRUCTION_RAM[16]  <= 32'b0000000_00010_00011_010_01000_0110011;   /*SLT reg[8] */
	// 	INSTRUCTION_RAM[20]  <= 32'b0000000_00010_00001_011_01001_0110011;   /*SLTU reg[9]*/
	// 	INSTRUCTION_RAM[24]  <= 32'b0000000_00010_00011_011_01010_0110011;   /*SLTU reg[10]*/
	// 	INSTRUCTION_RAM[28]  <= 32'b0000000_00010_00001_100_01011_0110011;   /*XOR  reg[11]*/
	//     INSTRUCTION_RAM[32]  <= 32'b0000000_00010_00011_100_01100_0110011;   /*XOR  reg[12]*/
	// 	INSTRUCTION_RAM[36]  <= 32'b0000000_00001_00011_101_01101_0110011;   /*SRL  reg[13]*/
	// 	INSTRUCTION_RAM[40]  <= 32'b0000000_00010_00011_101_01110_0110011;	/*SRL  reg[14]*/
	// 	INSTRUCTION_RAM[44]  <= 32'b0000000_00010_00001_110_01111_0110011;   /*OR   reg[15]*/
	// 	INSTRUCTION_RAM[48]  <= 32'b0000000_00010_00011_110_10000_0110011;   /*OR   reg[16]*/
	// 	INSTRUCTION_RAM[52]  <= 32'b0000000_00010_00001_111_10001_0110011;
	// 	INSTRUCTION_RAM[56]  <= 32'b0000000_00011_00001_111_10010_0110011;
	// 	 //immidiate instructions
	// 	INSTRUCTION_RAM[60]  <= 32'b000000000010_00001_010_10011_0010011;
	// 	INSTRUCTION_RAM[64]  <= 32'b000000000111_00011_010_10100_0010011;
	// 	INSTRUCTION_RAM[68]  <= 32'b000000000011_00001_011_10101_0010011;
	// 	INSTRUCTION_RAM[72]  <= 32'b000000000111_00011_011_10110_0010011;
	// 	INSTRUCTION_RAM[76]  <= 32'b010110101010_00011_100_10111_0010011;
	// 	INSTRUCTION_RAM[80]  <= 32'b010110101010_00011_111_11000_0010011;
	// 	INSTRUCTION_RAM[84]  <= 32'b0000000_00001_00011_001_11001_0010011;
	// 	INSTRUCTION_RAM[88]  <= 32'b0000000_11110_00001_001_11001_0010011;
	// 	INSTRUCTION_RAM[92]  <= 32'b0000000_00010_00010_101_11010_0010011;
	// 	INSTRUCTION_RAM[96]  <= 32'b0000000_00101_00011_101_11011_0010011;
	// 	INSTRUCTION_RAM[100] <= 32'b010110101010_00011_110_11100_0010011;
	// 	INSTRUCTION_RAM[104] <= 32'b010110101010_00000_110_11101_0010011;
	// 	INSTRUCTION_RAM[108] <= 32'b00000000000000000001_11110_0110111;
	// 	INSTRUCTION_RAM[112] <= 32'b00000000000000000001_11111_0010111;
	// 	// // // //SW
	// 	INSTRUCTION_RAM[120] <= 32'b0000000_00000_00011_010_00100_0100011;	
	// 	INSTRUCTION_RAM[116] <= 32'b0000000_00000_00010_010_00000_0100011;	
	// 	// // // //LW
	// 	INSTRUCTION_RAM[124] <= 32'b000000000000_00000_010_11111_0000011;
	// 	INSTRUCTION_RAM[128] <= 32'b000000000100_00000_010_11110_0000011;



	// 	/*
	// 	* Branch templates
	// 	* i[12]_1[10:5]_rs2_rs1_fucnt3_i[4:1]_i[11]_1100011
	// 	*
	// 	* BEQ:  x_xxxxxx_xxxxx_xxxxx_000_xxxx_x_1100011;
	// 	* BNE:  x_xxxxxx_xxxxx_xxxxx_001_xxxx_x_1100011;
	// 	* BLT:  x_xxxxxx_xxxxx_xxxxx_100_xxxx_x_1100011;
	// 	* BGE:  x_xxxxxx_xxxxx_xxxxx_101_xxxx_x_1100011;
	// 	* BLTU: x_xxxxxx_xxxxx_xxxxx_110_xxxx_x_1100011;
	// 	* GBEU: x_xxxxxx_xxxxx_xxxxx_111_xxxx_x_1100011;
	// 	*/ 



	// 	// /*Branch instructions*/
	// 	INSTRUCTION_RAM[0]  = 32'b0_000000_00000_00001_001_0100_0_1100011;	/*BNE rs1 != (rs2)0 PC + 8 = 8 */
	// 	// INSTRUCTION_RAM[4]  = 32'b0_000000_00010_00010_000_0010_0_1100011;	/*BEQ (rs2)0 == rs2 PC + 4 =8 (does nothing) */
	// 	// INSTRUCTION_RAM[8]  = 32'b1_111111_00010_00001_100_1110_1_1100011;	/*BLT rs1 < rs2 PC + (-4) = 4*/
	// 	// INSTRUCTION_RAM[12] = 32'b0_000000_00000_00011_101_0100_0_1100011;  /*BGE rs1 > rs2 PC + 8 = 20*/
	// 	// INSTRUCTION_RAM[16] = 32'b0; 
	// 	// INSTRUCTION_RAM[20] = 32'b0;
		
	// 	/*JALR instruction*/
	// 	//INSTRUCTION_RAM[0] = 32'b000000001101_00000_000_00101_1100111;

	// end
	// always@(reset) begin
	// 		//INSTRUCTION_RAM[0]  <= 32'b111111111111_00011_000_00011_0010011;    /*addi x3 = x3 - 1*/
	// 		//INSTRUCTION_RAM[4]  <= 32'b111111111111_00011_000_00011_0010011;    /*addi x3 = x3 - 1*/
	// 		INSTRUCTION_RAM[0]   <= 32'b0_000000_00011_00000_000_1010_0_1100011;		/*BEQ x3 == 0*/


	// 		//INSTRUCTION_RAM[0]  <= 32'b0000000_00010_00001_110_01111_0110011;   /*OR   reg[15] just an or for testing*/



	// 		INSTRUCTION_RAM[4]   <= 32'b0000000_00101_00100_000_00100_0110011;    	/*add x4 = x4 + x5*/
	// 		INSTRUCTION_RAM[8]   <= 32'b0100000_00101_00100_000_00101_0110011;  		 //sub: x5 = x4 - x5
	// 		INSTRUCTION_RAM[12]  <= 32'b111111111111_00011_000_00011_0010011;    /*addi x3 = x3 - 1*/
	// 		INSTRUCTION_RAM[16]   <= 32'b1_1111111000_1_11111111_00000_1101111; /* jump */
	// 		INSTRUCTION_RAM[20]  <= 32'b0000000_00000_00100_010_00100_0100011;	/*mem[0] = x4*/

	// 		// INSTRUCTION_RAM[0] <= 32'b0000000_00001_00010_000_00001_0110011;
	// 		// INSTRUCTION_RAM[4] <= 32'b0100000_00100_00011_000_00011_0110011;
	// 		// INSTRUCTION_RAM[8]   <= 32'b0100000_00101_00100_000_00101_0110011; 
			
		   

	// end

endmodule
