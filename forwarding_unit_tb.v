/*
* fowarding_unit_tb.v
*/

module fowarding_unit_tb;
   reg[4:0] MEM_writeReg, WB_writeReg, EX_rs1, EX_rs2;
   reg MEM_WriteF, WB_WriteFf;
   wire[1:0] rs1_forward, rs2_forward;

    
   fowarding_unit FU(
       .MEM_writeReg(MEM_writeReg),
       .WB_writeReg(WB_writeReg),
       .EX_rs1(EX_rs1),
       .EX_rs2(EX_rs2),
       .MEM_WriteF(MEM_WriteF),
       .WB_WriteFf(WB_WriteFf),
       .rs1_forward(rs1_forward),
       .rs2_forward(rs2_forward)
   
   );



    initial begin
        $display("#################################################################");
        $display("\t\t fowarding unit test                            #");
        $display("#################################################################");

        MEM_writeReg = 5'b00001;
        WB_writeReg  = 5'b00011;
        EX_rs1       = 5'b00001;
        EX_rs2       = 5'b00011;
        MEM_WriteF   = 1;
        WB_WriteFf   = 0;

        #10;
        $display("MEM_writeReg(%d) | MEM_WriteF(%b)  | WB_writeReg(%d) | WB_WriteFf(%b)  | EX_rs1(%d) | EX_rs2(%d) | rs1_forward(%b) | rs2_forward(%b)",MEM_writeReg, MEM_WriteF, WB_writeReg,WB_WriteFf, EX_rs1, EX_rs2, rs1_forward, rs2_forward);

        #10;

        MEM_WriteF = 0;
        WB_WriteFf = 1;

        #10;
        $display("MEM_writeReg(%d) | MEM_WriteF(%b)  | WB_writeReg(%d) | WB_WriteFf(%b)  | EX_rs1(%d) | EX_rs2(%d) | rs1_forward(%b) | rs2_forward(%b)",MEM_writeReg, MEM_WriteF, WB_writeReg,WB_WriteFf, EX_rs1, EX_rs2, rs1_forward, rs2_forward);

        WB_writeReg = 5'b00001;
         
        #10;
        $display("MEM_writeReg(%d) | MEM_WriteF(%b)  | WB_writeReg(%d) | WB_WriteFf(%b)  | EX_rs1(%d) | EX_rs2(%d) | rs1_forward(%b) | rs2_forward(%b)",MEM_writeReg, MEM_WriteF, WB_writeReg,WB_WriteFf, EX_rs1, EX_rs2, rs1_forward, rs2_forward);

        MEM_writeReg = 5'b00011;
        MEM_WriteF   = !MEM_WriteF;
        WB_WriteFf   = !WB_WriteFf;
         
        #10;
        $display("MEM_writeReg(%d) | MEM_WriteF(%b)  | WB_writeReg(%d) | WB_WriteFf(%b)  | EX_rs1(%d) | EX_rs2(%d) | rs1_forward(%b) | rs2_forward(%b)",MEM_writeReg, MEM_WriteF, WB_writeReg,WB_WriteFf, EX_rs1, EX_rs2, rs1_forward, rs2_forward);

        WB_WriteFf = 1;
         
        #10;
        $display("MEM_writeReg(%d) | MEM_WriteF(%b)  | WB_writeReg(%d) | WB_WriteFf(%b)  | EX_rs1(%d) | EX_rs2(%d) | rs1_forward(%b) | rs2_forward(%b)",MEM_writeReg, MEM_WriteF, WB_writeReg,WB_WriteFf, EX_rs1, EX_rs2, rs1_forward, rs2_forward);


        


    end




endmodule
