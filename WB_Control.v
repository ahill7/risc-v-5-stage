/*
* WB_Control.v
* Andrew Hill
*/

module WB_Control(
	input[31:0] ALU_Result,
	input regWriteF,
	input[4:0] writeReg,
    input[31:0]	readData,
    input[31:0]	incr_PC, //curr_PC,
	//input[31:0] branch_PC, jump,
	input[1:0] memToRegF,// PC_SelectF,
	output [31:0] writeData,
	//output [31:0] to_PC,
	output WB_regWriteF,
	output[4:0] rd
);
wire[31:0] JALR_Result = {ALU_Result[31:1], 1'b0};

	Threeto1Mux Mem_Selector(
		.genericInput1(ALU_Result),
		.genericInput2(readData),
		.genericInput3(incr_PC),
		.genericFlag(memToRegF),
		.genericOutput(writeData)
	);

	//Threeto1Mux PC_Selector(
	//	.genericInput1(incr_PC), 	/*PC_SelectF = 2'b00*/
	//	.genericInput2(branch_PC),  /*PC_SelectF = 2'b01*/
	//	.genericInput3(jump),		/*PC_SelectF = 2'b10*/ 
	//	.genericFlag(PC_SelectF),
	//	.genericOutput(to_PC)
	//);
	
	// mux4to1 PC_Selector(
	// 	.gen0(incr_PC),		/*PC_SelectF = 2'b00*/
	// 	.gen1(branch_PC),   /*PC_SelectF = 2'b01*/
	// 	.gen2(jump),		/*PC_SelectF = 2'b10*/
	// 	.gen3(JALR_Result),	/*PC_SelectF = 2'b11*/
	// 	.sel(PC_SelectF), 
	// 	.gen_out(to_PC)
	// );

	assign WB_regWriteF = regWriteF;
	assign rd = writeReg;

endmodule
