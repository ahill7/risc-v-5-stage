/*
* RISC_V_PIPELINE_CPU.v
*/


module RISC_V_PIPELINE_CPU(
input clock,
output[31:0] result

);
/*__________________________________________________________________________________*/
/*______________________________________ Wires _____________________________________*/
/*__________________________________________________________________________________*/
	reg[31:0] Cycles;

	parameter[6:0] Branches	= 7'b1100011;
	parameter[2:0] BEQ  = 3'b000,
	 			    BNE  = 3'b001,
				    BLT  = 3'b100,
				    BGE  = 3'b101,
				    BLTU = 3'b110,
				    BGEU = 3'b111;

	reg DEBUG=0;
	reg[1:0] state;
	localparam IDLE=2'b00, CLEAR=2'b01, INIT=2'b10, RUN=2'b11;
	reg branch_cond_fail;


	/*Fetch registers and wires*/ 
		reg[31:0] to_PC;
		reg reset;
		wire[31:0] incr_PC, instruction, from_PC;
		wire[6:0] opcode;
		wire[4:0] IF_rs1, IF_rs2;
		//reg[31:0] incr_PC = IF_incr_PC + 4;
				


	/*Control registers and wires*/
		//wire[6:0] control_opcode = instruction[6:0];
		//wire[6:0] func7 = instruction[31:25];
		/*outtput*/
		wire[1:0] PC_SelectF; 
		wire regWriteF;
		wire[1:0] regDestF;
		wire[2:0] ALU_a_MUXF;
		wire[1:0] ALU_b_MUXF;
		wire[1:0] ALU_opF;
		wire memWriteF, memReadF;
		wire[1:0] writeback_selectF;
		wire memMUXF, memWriteMUXF;


	/*Decode registers and wires*/
		wire[31:0] writeData;
		wire[31:0] extended2, immediate_value, branch, jump_PC, outRegA, outRegB, from_zero, from_zero_large;
		wire[31:0] curr_PC;
		wire[2:0]  funct3;
		wire[6:0]  funct7;


	/*Execute registers and wires*/
		wire[31:0] ALU_Result, operandA;
		wire[31:0] branch_PC;


	/*Memory registers and Wires*/
	    wire[31:0] address;
		wire[31:0] readData, memWriteData;
		

	/*Write Back registers and wires */
		wire[31:0] new_PC;
		wire[4:0] rd;
		wire WB_regWriteF;

	/* Hazard Control Wires */
		wire HC_regWriteF;
		wire stall;
	



/*-----------------------------------------------------------------------------------------------*/
/*______________________________________ Pipeline Registers _____________________________________*/
/*-----------------------------------------------------------------------------------------------*/



	/*IF/ID Pipeline registers*/
	reg[31:0] IFID_instruction;
	reg[31:0] IFID_from_PC;
	reg[31:0] IFID_incr_PC;
	reg[31:0] IFID_jump_PC;
	reg[4:0]  IFID_writeReg; 
	//reg[1:0]  IFID_regDestF; /* Isnt used anywhere, figure out its purpose again*/
	reg[6:0]  IFID_opcode;
	reg[4:0]  IFID_rs1, IFID_rs2;
	//reg		  IFID_regWriteF;

	

	//flags
	reg[1:0]  IFID_PC_SelectF;
	reg 	  IFID_regWriteF, IFID_memMUXF, IFID_memWriteMUXF;
	reg[1:0]  IFID_regDestF;
	reg[2:0]  IFID_ALU_a_MUXF;
	reg[1:0]  IFID_ALU_b_MUXF;
	reg[1:0]  IFID_ALU_opF;
	reg       IFID_memWriteF;
	reg       IFID_memReadF; 
	reg[1:0]  IFID_writeback_selectF;



	/*Wires*/
	wire[6:0]  IF_opcode	   = IFID_opcode;
	wire[31:0] IF_incr_PC      = IFID_incr_PC;
	wire[31:0] IF_jump_PC	   = IFID_jump_PC;
	wire[4:0]  IF_writeReg     = IFID_writeReg; 
	wire[2:0]  IF_ALU_a_MUXF   = IFID_ALU_a_MUXF;
	wire[1:0]  IF_ALU_b_MUXF   = IFID_ALU_b_MUXF;
	wire[1:0]  IF_ALU_opF      = IFID_ALU_opF;
	wire	   IF_regWriteF    = IFID_regWriteF;
	wire	   IF_memWriteMUXF = IFID_memWriteMUXF;
	wire 	   IF_memReadF     = IFID_memReadF;
	wire       IF_memWriteF    = IFID_memWriteF;
	wire       IF_memMUXF      = IFID_memMUXF;
	wire[1:0]  IF_writeback_selectF = IFID_writeback_selectF;
	wire[31:0] IF_prev_PC           = to_PC;
    wire[31:0] IF_instruction       = IFID_instruction;
    

    /*forwarding wires*/
    wire[1:0] rs1_forwardF, rs2_forwardF;

	/*ID/EX pipeline regiesters*/
    reg[31:0] IDEX_instruction;//
	reg[31:0] IDEX_curr_PC;//
	reg[31:0] IDEX_extended2;//
	reg[31:0] IDEX_immediate_value;//
	reg[31:0] IDEX_branch; //
	reg[31:0] IDEX_outRegA, IDEX_outRegB;//
	reg[31:0] IDEX_jump_PC;//
	reg[31:0] IDEX_from_zero_large;//
	reg[31:0] IDEX_incr_PC;//
	reg[2:0]  IDEX_funct3;//
	reg[6:0]  IDEX_funct7;//
	reg[6:0]  IDEX_opcode;//
	reg[4:0]  IDEX_writeReg;//
	reg[2:0]  IDEX_ALU_a_MUXF;//
	reg[1:0]  IDEX_ALU_b_MUXF;//
	reg[1:0]  IDEX_ALU_opF;//
	reg[1:0]  IDEX_writeback_selectF;//
	reg 	  IDEX_regWriteF;//
	reg 	  IDEX_memWriteMUXF;//
	reg 	  IDEX_memMUXF;//
	reg       IDEX_memWriteF;//
	reg		  IDEX_memReadF;//
    /*Forwarding Registers*/
    reg[1:0] IDEX_rs1_forward, IDEX_rs2_forward;

	

	/*wires between EX and MEM*/
    wire[31:0] ID_instruction       = IDEX_instruction; 
    wire[31:0] ID_branch            = IDEX_branch; 
    wire[31:0] ID_jump_PC           = IDEX_jump_PC; 
    wire[31:0] ID_from_zero_large   = IDEX_from_zero_large; 
	wire[31:0] ID_outRegB           = IDEX_outRegB;
    wire[31:0] ID_outRegA           = IDEX_outRegA;
	wire[31:0] ID_incr_PC		  	= IDEX_incr_PC;
    wire[31:0] ID_curr_PC           = IDEX_curr_PC;
  	wire[31:0] ID_extended2 	  	= IDEX_extended2;
	wire[31:0] ID_immediate_value   = IDEX_immediate_value;
    wire[6:0]  ID_funct7            = IDEX_funct7; 
	wire[6:0]  ID_opcode            = IDEX_opcode;
	wire[4:0]  ID_writeReg  	    = IDEX_writeReg;
    wire[2:0]  ID_funct3            = IDEX_funct3; 
	wire[2:0]  ID_ALU_a_MUXF        = IDEX_ALU_a_MUXF;
	wire[1:0]  ID_ALU_b_MUXF        = IDEX_ALU_b_MUXF;
	wire[1:0]  ID_ALU_opF           = IDEX_ALU_opF;
	wire[1:0]  ID_writeback_selectF = IDEX_writeback_selectF;
	wire 	   ID_memMUXF           = IDEX_memMUXF;
	wire	   ID_memReadF 		    = IDEX_memReadF;
	wire	   ID_regWriteF		    = IDEX_regWriteF;
	wire 	   ID_memWriteF		    = IDEX_memWriteF;
	wire 	   ID_memWriteMUXF      = IDEX_memWriteMUXF;
    wire[1:0]  ID_rs1_forward       = IDEX_rs1_forward;
    wire[1:0]  ID_rs2_forward       = IDEX_rs2_forward;



  	/*EX/MEM pipeline regiesters*/
	reg[31:0] EXMEM_incr_PC;
   	reg[31:0] EXMEM_memWriteData;
   	reg[31:0] EXMEM_ALU_Result;
   	reg[31:0] EXMEM_extended2;
   	reg[31:0] EXMEM_immediate_value;
	reg[31:0] EXMEM_outRegB, EXMEM_outRegA;
	reg[4:0]  EXMEM_writeReg;
  	reg		  EXMEM_memMUXF;
   	reg		  EXMEM_memReadF;
	reg 	  EXMEM_regWriteF;
	reg[1:0]  EXMEM_writeback_selectF;
	reg 	  EXMEM_memWriteF;
	reg 	  EXMEM_memWriteMUXF;



	/*wires for Mem to Wb registers*/
	wire[31:0] MEM_ALU_Result = EXMEM_ALU_Result;
	wire[31:0] MEM_incr_PC    = EXMEM_incr_PC;
	wire 	   MEM_regWriteF  = EXMEM_regWriteF;
	wire[4:0]  MEM_writeReg   = EXMEM_writeReg;
	wire[1:0]  MEM_writeback_selectF = EXMEM_writeback_selectF;
	/*MEM to WB Registers*/	
	reg[31:0] MEMWB_ALU_Result;
	reg[31:0] MEMWB_readData;
	reg[31:0] MEMWB_incr_PC;
	reg[31:0] MEMWB_writeData;
	reg 	  MEMWB_regWriteF;
	reg[4:0]  MEMWB_writeReg;
	reg[4:0]  MEMWB_rd;
	reg[1:0]  MEMWB_writeback_selectF;
	


	always@(posedge clock) begin
		if(reset) begin
            to_PC <=0;
            state <= INIT;
            reset <= 0;

		end
		else begin
			case(state)
				IDLE:begin
                    state <= IDLE;
                    to_PC <= to_PC;
				end
				CLEAR:begin
	                IFID_instruction       <= 0;
	                IFID_from_PC           <= 0;
	                IFID_incr_PC           <= 0;
	                IFID_jump_PC           <= 0;
                    IFID_writeReg          <= 0; 
	                IFID_opcode            <= 0;
	                IFID_rs1               <= 0;
                    IFID_rs2               <= 0;
	                IFID_PC_SelectF        <= 0;
	                IFID_regWriteF         <= 0;
                    IFID_memMUXF           <= 0;
                    IFID_memWriteMUXF      <= 0;
	                IFID_regDestF          <= 0;
	                IFID_ALU_a_MUXF        <= 0;
	                IFID_ALU_b_MUXF        <= 0;
	                IFID_ALU_opF           <= 0;
	                IFID_memWriteF         <= 0;
	                IFID_memReadF          <= 0; 
	                IFID_writeback_selectF <= 0;

                    state                  <= 2'b00;




				end
				INIT:begin
					$display("\n Initializing Values\n");
					/*Intalize Variables*/
					to_PC  <= from_PC;
					state  <= RUN;
					Cycles <= 0;
					//DEBUG  <=1;
                    /*Initalizing registers for Fibbonacci*/
		            ID_Control1.Register_File1.reg_file[0] <= 0;
		            ID_Control1.Register_File1.reg_file[1] <=2;
		            ID_Control1.Register_File1.reg_file[2] <=4;
		            ID_Control1.Register_File1.reg_file[3] <= 11; // Fib of reg[3].
            		ID_Control1.Register_File1.reg_file[4] <= 1;
		            ID_Control1.Register_File1.reg_file[5] <= -1;



                    /*Initalizes instrution Memory.*/
                    IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[0]   <= 32'b0_000000_00011_00000_000_1010_0_1100011; //BEQ: if x3 == 0;
			        IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[4]   <= 32'b0000000_00101_00100_000_00100_0110011;   /*add x4 = x4 + x5*/
			        IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[8]   <= 32'b0100000_00101_00100_000_00101_0110011; 	 //sub: x5 = x4 - x5
		            IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[12]  <= 32'b111111111111_00011_000_00011_0010011;    /*addi x3 = x3 - 1*/
			        IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[16]  <= 32'b1_1111111000_1_11111111_00000_1101111;   /* jump */
			        IF_Control1.Instruction_Memory1.INSTRUCTION_RAM[20]  <= 32'b0000000_00000_00100_010_00100_0100011;	 /*mem[0] = x4*/



				end
				RUN:begin
					/*Fetch-Decode registers */
					IFID_opcode			   <= opcode;
					IFID_rs1 			   <= IF_rs1;
					IFID_rs2 			   <= IF_rs2;
					IFID_instruction 	   <= instruction;
					IFID_from_PC 		   <= from_PC;
					/*Data hazard*/
					//if(stall) begin
					//	to_PC		       <= from_PC ;
					//end else begin
					if(branch_cond_fail) begin
						to_PC              <= EXMEM_incr_PC;    /*branch fials*/
                        $display("\n\n Branch fail \n\n");

					end
                    //else if(stall) begin  /*Data hazard: TODO: fix*/
						//to_PC		       <= 8;
                    //end
					else begin
						to_PC		       <= from_PC; /*Treated as PC+4, rename*/
					end
					//IFID_incr_PC           <= to_PC + 4;
					//end
					IFID_writeReg	       <= instruction[11:7];
					IFID_incr_PC		   <= incr_PC;
			
					/*Flags*/
					IFID_PC_SelectF        <= PC_SelectF;
					IFID_regWriteF		   <= regWriteF;
					IFID_memMUXF		   <= memMUXF;
					IFID_memWriteMUXF      <= memWriteMUXF;
					IFID_regDestF		   <= regDestF;
					IFID_ALU_a_MUXF 	   <= ALU_a_MUXF;
					IFID_ALU_b_MUXF 	   <= ALU_b_MUXF;
					IFID_ALU_opF		   <= ALU_opF;
					IFID_memWriteF         <= memWriteF;
					IFID_memReadF		   <= memReadF;
					IFID_writeback_selectF <= writeback_selectF;
					IFID_jump_PC 		   <= jump_PC;
			
			
					/*Decode-Execute Registers*/
                    IDEX_instruction       <= IF_instruction;
					IDEX_opcode			   <= IF_opcode;
					IDEX_curr_PC 		   <= IF_incr_PC;
					IDEX_extended2 		   <= extended2;
					IDEX_immediate_value   <= immediate_value;
					IDEX_branch            <= branch;

					if(rs2_forwardF == 2'b10) IDEX_outRegA <= ALU_Result;
					else if(rs2_forwardF == 2'b01) IDEX_outRegA <= MEM_ALU_Result; /*TODO: Make sure this is the right value to assign here*/
					else IDEX_outRegA 		   <= outRegA;

					if(rs1_forwardF == 2'b10) IDEX_outRegB <= ALU_Result;
					else if(rs1_forwardF == 2'b01) IDEX_outRegB <= MEM_ALU_Result; /*TODO: Make sure this is the right value to assign here*/
					else IDEX_outRegB 		   <= outRegB;

					IDEX_jump_PC		   <= IF_jump_PC; 
					IDEX_from_zero_large   <= from_zero_large;
					IDEX_funct3            <= funct3;
					IDEX_funct7			   <= funct7;
					IDEX_incr_PC 		   <= IF_incr_PC;
					IDEX_writeReg		   <= IF_writeReg;
					IDEX_ALU_a_MUXF	       <= IF_ALU_a_MUXF;
					IDEX_ALU_b_MUXF		   <= IF_ALU_b_MUXF;
					IDEX_regWriteF		   <= IF_regWriteF;
					IDEX_memWriteMUXF  	   <= IF_memWriteMUXF;
					IDEX_memWriteF         <= IF_memWriteF;
					IDEX_memReadF		   <= IF_memReadF;
					IDEX_memMUXF		   <= IF_memMUXF;
					IDEX_regWriteF	       <= IF_regWriteF;
					IDEX_writeback_selectF <= IF_writeback_selectF;
                    IDEX_rs1_forward       <= rs1_forwardF;
                    IDEX_rs2_forward       <= rs2_forwardF;



                    /*
                    * Decode with stall
                    IDEX_instruction       <= (stall) ? ID_incr_PC : IF_instruction;
					IDEX_opcode			   <= (stall) ? ID_opcode :  IF_opcode;
					IDEX_curr_PC 		   <= (stall) ? ID_incr_PC :   IF_incr_PC;
					IDEX_extended2 		   <= (stall) ? ID_extended2 : extended2;
					IDEX_immediate_value   <= (stall) ? ID_immediate_value : immediate_value;
					IDEX_branch            <= (stall) ? ID_branch :  branch;
					IDEX_outRegA 		   <= (stall) ? ID_outRegA : outRegA;
					IDEX_outRegB 		   <= (stall) ? ID_outRegB : outRegB;
					IDEX_jump_PC		   <= (stall) ? ID_jump_PC : IF_jump_PC; 
					IDEX_from_zero_large   <= (stall) ? ID_from_zero_large : from_zero_large;
					IDEX_funct3            <= (stall) ? ID_funct3 : funct3;
					IDEX_funct7			   <= (stall) ? ID_funct7 : funct7;
					IDEX_incr_PC 		   <= (stall) ? ID_incr_PC : IF_incr_PC;
					IDEX_writeReg		   <= (stall) ? ID_writeReg:      IF_writeReg;
					IDEX_ALU_a_MUXF	       <= (stall) ? ID_ALU_a_MUXF :      IF_ALU_a_MUXF;
					IDEX_ALU_b_MUXF		   <= (stall) ? ID_ALU_b_MUXF : IF_ALU_b_MUXF;
					IDEX_regWriteF		   <= (stall) ? ID_regWriteF :      IF_regWriteF;
					IDEX_memWriteMUXF  	   <= (stall) ? ID_memWriteF :     IF_memWriteMUXF;
					IDEX_memWriteF         <= (stall) ? ID_memWriteF :        IF_memWriteF;
					IDEX_memReadF		   <= (stall) ? ID_memReadF :       IF_memReadF;
					IDEX_memMUXF		   <= (stall) ? ID_memMUXF :      IF_memMUXF;
					IDEX_regWriteF	       <= (stall) ? ID_regWriteF :       IF_regWriteF;
					IDEX_writeback_selectF <= (stall) ? ID_writeback_selectF :       IF_writeback_selectF;
                    IDEX_rs1_forward       <= (stall) ? ID_rs1_forward :     rs1_forwardF;
                    IDEX_rs2_forward       <=  (stall) ? ID_rs2_forward :    rs2_forwardF;

                    *
                     */


			
	    			/*Execue-Memory Registers*/	
					EXMEM_extended2		   <= ID_extended2;
					EXMEM_immediate_value  <= ID_immediate_value;
					EXMEM_memWriteF        <= ID_memWriteF;
					EXMEM_memReadF		   <= ID_memReadF;
					EXMEM_memWriteMUXF	   <= ID_memWriteMUXF;
					EXMEM_memMUXF          <= ID_memMUXF;
					EXMEM_ALU_Result       <= ALU_Result; 
					EXMEM_outRegB 		   <= ID_outRegB;
					EXMEM_outRegA     	   <= ID_outRegA;
					EXMEM_regWriteF        <= ID_regWriteF;
					EXMEM_writeReg		   <= ID_writeReg;
					EXMEM_writeback_selectF<= ID_writeback_selectF;
					EXMEM_incr_PC          <= ID_incr_PC;
			
					
			
					/*Memory-WriteBack registers*/
					MEMWB_ALU_Result       <= MEM_ALU_Result;
					MEMWB_readData         <= readData;
					MEMWB_regWriteF	       <= MEM_regWriteF;
					MEMWB_writeReg         <= MEM_writeReg;
					MEMWB_writeback_selectF <= MEM_writeback_selectF;
					MEMWB_rd				<= rd;
					MEMWB_incr_PC		   <= MEM_incr_PC;

                    state <= RUN;
				end
			default:begin
				state <= IDLE;
			end
			endcase
		end



		$display("\nCycles(%d)", Cycles);
		//$display("branch_cond_fail(%b)",branch_cond_fail);
		$display("IF: opcode(%b) | curr_PC(%d) | instuction(%b) | writeback_selectF(%b) | incr_PC(%d)", opcode, to_PC, IF_Control1.instruction, writeback_selectF, incr_PC);
		//$display("branch(%d) = branch_PC(%d) + from_PC(%d)", IF_Control1.branch_target1.branch, IF_Control1.branch_target1.branch_PC, IF_Control1.branch_target1.from_PC);
        $display("stadd(%d), branch_cond_fail(%d)", stall, branch_cond_fail);
		$display("IF_Control1.branch(%d)",IF_Control1.from_PC);
		$display("ID: opcode(%b) | incr_PC(%d) | writeback_selectF(%b) |  rs1(%d) | rs2(%d) | write_data(%d) | writeF(%b) | writeReg(%d) | regWF(%b) | write_reg(%d)", IFID_opcode ,IFID_incr_PC, IFID_writeback_selectF, uut.outRegA, uut.outRegB, uut.ID_Control1.Register_File1.writeData, uut.ID_Control1.Register_File1.regWriteF, uut.ID_Control1.Register_File1.writeReg, uut.regWriteF, uut.IFID_writeReg);
		$display("EX: opcode(%b) | incr_PC(%d) |  writeback_selectF(%b) | AMUX(%b) | rs1(%d) | rs(%d) = ALU_Result(%d) |immediate value(%d) | RegWF(%b) | writeReg(%d)", IDEX_opcode,IDEX_curr_PC, IDEX_writeback_selectF,  uut.IFID_ALU_a_MUXF, IDEX_outRegA, IDEX_outRegB, uut.ALU_Result, uut.IDEX_immediate_value, uut.IDEX_regWriteF, uut.IDEX_writeReg);
        $display("EX2: funct7(%b) | funct(%b) | rs1F(%b) | rs2F(%b) | MEM_ALUR", IDEX_funct7, IDEX_funct3, IDEX_rs1_forward, IDEX_rs2_forward, MEM_ALU_Result);
		$display("MEM: incr_PC(%d) | writeback_selectF(%b) | memWF(%b) | memMUXF(%b) | memWriteMUXF(%b) | memWriteD(%d) | address(%d)", EXMEM_incr_PC, EXMEM_writeback_selectF, EXMEM_memWriteF, EXMEM_memMUXF, EXMEM_memWriteMUXF, MEM_Control1.memWriteData, MEM_Control1.address);
		$display("MEM2: outRegB(%d) | memWriteData(%d)", MEM_Control1.outRegB, EXMEM_ALU_Result);
		$display("WB: incr_PC(%d) | WF(%b) | memToRegF(%b) | writeReg(%d) | writeData(%d)", MEMWB_incr_PC, uut.MEMWB_regWriteF, MEMWB_writeback_selectF, uut.MEMWB_writeReg, writeData);
		Cycles = Cycles + 1;
		end


/*--------------------------------------------------------------------------------------------------------------*/
/*______________________________________________ Wiring Processor ______________________________________________*/
/*--------------------------------------------------------------------------------------------------------------*/


	IF_Control IF_Control1(
		//inputs
		.to_PC(to_PC), 	 
		.PC_SelectF(PC_SelectF),
		.branch_PC(IDEX_branch), 
		.JALR_Result(ALU_Result),
		.clock(clock),
		.reset(reset),

		//output
		.instruction(instruction),
		.from_PC(from_PC),
		.rs1(IF_rs1),
		.rs2(IF_rs2),
		.opcode(opcode),
		.incr_PC(incr_PC),
		.jump_PC(jump_PC)
	);


  
	control_unit CU1(
		/*input*/
		.control_opcode(instruction[6:0]),
		.func7(instruction[31:25]),

		/*output*/
		.memWriteMUXF(memWriteMUXF),
		.PC_SelectF(PC_SelectF),
		.memMUXF(memMUXF),
		.regWriteF(regWriteF),
		.regDestF(regDestF),
		.ALU_a_MUXF(ALU_a_MUXF),
		.ALU_b_MUXF(ALU_b_MUXF),
		.ALU_opF(ALU_opF),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.writeback_selectF(writeback_selectF)
	);



	ID_Control ID_Control1(
		//input
		.instruction(IFID_instruction),
		.from_PC(IFID_from_PC),
		.rd(MEMWB_writeReg), /*From Write back, no pipeline reg yet*/
		.writeData(writeData), /*from write back, no pipeline reg yet*/
		.regWriteF(MEMWB_regWriteF), /*From write back, no pipeline reg yet*/ 
		.clock(clock),
		.regDestF(IFID_regDestF),

		//output
		.extended2(extended2),
		.immediate_value(immediate_value),
		.branch(branch),
		.outRegA(outRegA),
		.outRegB(outRegB),
		.from_zero(from_zero),
		.from_zero_large(from_zero_large),
		.PC(curr_PC),
		.funct3(funct3),
        .funct7(funct7)
	);



	hazard_control HC(
        //input
		.curr_rs1(IFID_rs1),
		.curr_rs2(IFID_rs2),
		.opcode(IFID_opcode),
		.EX_rd(IDEX_writeReg),
		.MEM_rd(EXMEM_writeReg),
		.inRegWriteF(EXMEM_regWriteF),

        //output
		.regWriteF(HC_regWriteF),
		.stall(stall)	
	); 


    /*
    * TODO:
    * 1.) Make sure I didnt get rs1 and rs2 switched.
    * 2.) Make sure rs1 and rs2 are for the right instruction, they might be comming from the next instruction.
    */


    fowarding_unit FW(
    //input
    .MEM_writeReg(IDEX_writeReg),
    .WB_writeReg(EXMEM_writeReg),
	.Cycles(Cycles),
    .EX_rs1(IFID_instruction[19:15]), /*Make sure I didnt get rs1 and rs2 switched*/
    .EX_rs2(IFID_instruction[24:20]), /**/
    .MEM_WriteF(IDEX_regWriteF),
    .WB_WriteFf(EXMEM_regWriteF),
    .rs1_forward(rs1_forwardF),
    .rs2_forward(rs2_forwardF)
    
    );



	EX_Control EX_Control1(
		//input
		.PC(IDEX_curr_PC),
		.extended2(IDEX_extended2),
		.immediate_value(IDEX_immediate_value),
		.branch(IDEX_branch),
		.incr_PC(IDEX_incr_PC), /*From IF, not done yet */
		.outRegA(IDEX_outRegA),
		.outRegB(IDEX_outRegB),
		.from_zero(IDEX_jump_PC),/* This is the jump pc, rename this */
		.from_zero_large(IDEX_from_zero_large),
		.ALU_a_MUXF(IDEX_ALU_a_MUXF), /*FROM CU, not done yet*/
		.ALU_opF(IDEX_ALU_opF), /* From CU, not done yet*/
		.funct3(IDEX_funct3), /*From fetch, may need to pass whole instruction through ID or jsut this specific value. though ID.*/
		.funct7(IDEX_funct7), /*From fetch or need to pass instrucion or specific index from instruction through ID.*/ 
		.ALU_b_MUXF(IDEX_ALU_b_MUXF), /* FROM CU, not done yet*/

		//output
		.zeroF(zeroF),
		.ALU_Result(ALU_Result),
		.operandA(operandA),
		.branch_PC(branch_PC)
	);



    /*Branch choice, if branch condition fails sets flag to set PC to incr after branch instruction and zeros out all write flags. 
     * If branch condition passes then continue.
     */	
	always@(posedge clock) begin 
		if(IDEX_opcode == Branches) begin
			if((zeroF && (IDEX_funct3 == BEQ || IDEX_funct3 == BGE || IDEX_funct3 == BGEU)) || (!zeroF && (IDEX_funct3 == BNE || IDEX_funct3 == BLT || IDEX_funct3 == BLTU)))begin
				branch_cond_fail <= 0; /*Want this to be false, other wise you add 4 to branch PCOA*/
				$display("\n if \n");
			end else begin
				branch_cond_fail        <= 1;
  				EXMEM_memMUXF           <= 0;
   				EXMEM_memReadF          <= 0;
				EXMEM_regWriteF         <= 0;
				EXMEM_writeback_selectF <= 0;
				EXMEM_memWriteF         <= 0;
				EXMEM_memWriteMUXF      <= 0; 
				$display("\n else \n");
			end	
		end
        else begin
	    	branch_cond_fail <= 0;
		end	
	end



	MEM_Control MEM_Control1(
        //input
		.extended2(EXMEM_extended2),
		.ALU_Result(EXMEM_ALU_Result),
		.outRegB(EXMEM_outRegB),
		.immediate_value(EXMEM_immediate_value),
		.memMUXF(EXMEM_memMUXF), /*Can replace, address now calculated above*/
		.memWriteMUXF(EXMEM_memWriteMUXF),
		.memWriteF(EXMEM_memWriteF),/*Dont need anymore*/
		.clock(clock),
		.memReadF(EXMEM_memReadF),

		//output
		.readData(readData)
	);

	
	
	WB_Control WB_Control1(
		//input
		.ALU_Result(MEMWB_ALU_Result),
		.readData(MEMWB_readData),
		.incr_PC(MEMWB_incr_PC),
		.regWriteF(MEMWB_regWriteF),
		.writeReg(MEMWB_writeReg),
		.memToRegF(MEMWB_writeback_selectF),

		//output
		.writeData(writeData),
		.WB_regWriteF(WB_regWriteF),
		.rd(rd)
	);
	

endmodule
