/*
* Reg_file_read.v
*/

module Reg_file_read(
	input[31:0] reg_file [0:31],
	input[31:0] in_rs1, in_rs2,
	output[31:0] rs1, rs2
);

	always@(in_rs1, in_rs2) begin
		if(!in_rs1) rs1 = 0;
		else rs1 = reg_file[in_rs1];
		if(!in_rs2) rs2 = 0;
		else rs2 = reg_file[in_rs2];
	end


endmodule
