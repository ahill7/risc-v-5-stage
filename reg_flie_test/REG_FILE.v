/*
* REG_FILE.v
*/

module REG_FILE(
	input clock
);

	reg[31:0] reg_file [31:0];
	reg[4:0] in_rs1, in_rs2, in_rd; 
	wire[31:0] write_data;
	reg[31:0] rs1, rs2;
	
	always@(in_rs1, in_rs2)begin
		rs1 = reg_file[in_rs1];	
		rs2 = reg_file[in_rs2];
	end
	assign write_data = rs1 + rs2;

endmodule
