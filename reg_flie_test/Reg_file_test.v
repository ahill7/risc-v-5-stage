/*
* Reg_file_TB.v
*/

module Register_File_TB;
	reg clock;

	REG_FILE uut(
		.clock(clock)
	);


	wire[31:0] ALU_Result;

	always begin
		#5 clock = !clock;
	end
	
	initial begin
	/* Iinitalize few regiester values*/
	uut.reg_file[0] =0;
	uut.reg_file[1] =2;
	uut.reg_file[2] =4;
	uut.reg_file[3] =2**32-1;

	uut.in_rs1 = 2;
	uut.in_rs2 = 1; 
#10;
	$display("rs1(%d) | rs2(%d) | write_data(%d)", uut.rs1, uut.rs2, uut.write_data);
	#10 $stop;
	end
	



endmodule
