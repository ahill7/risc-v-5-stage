/*
* CPU_hazard_TB.v
*/

module CPU_hazard_TB;

	reg clock;

	CPU_hazard_control uut(
		.clock(clock)
	);

	always begin
		#5 clock = !clock;
	end

	initial begin
	
	$display("\n#########################################################################################");
	$display("########################### CPU + Hazard Control Test Bed ###############################");
	$display("#########################################################################################\n");
	
	clock = 0;
	uut.curr_rs1 = 5'b00001;
	uut.curr_rs2 = 5'b00010;
	uut.EX_rd    = 5'b00011;	
	uut.MEM_rd   = 5'b00111;
	uut.reg_writeF = 1'b1;
	uut.opcode     = 7'b0110011;

	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	#10;	
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	uut.EX_rd    = 5'b00010;	
	#10
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	uut.MEM_rd = uut.EX_rd;
	uut.EX_rd = 5'b01010;
	#10
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	uut.MEM_rd = 5'b00111;
	uut.EX_rd = 5'b01010;
	uut.reg_writeF = 1'b1;
	#10
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	uut.curr_rs1 = 5'b00111;
	uut.MEM_rd = 5'b00111;
	uut.EX_rd = 5'b01010;
	#10
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	uut.MEM_rd = uut.EX_rd; 
	#10
	$display("\nrs1(%d), rs2(%d), EX_rd(%d), MEM_rd(%d)", uut.curr_rs1, uut.curr_rs2, uut.EX_rd, uut.MEM_rd);
	$display("reg_writeF(%b) | regWriteF(%b), PC(%d)", uut.reg_writeF, uut.regWriteF, uut.PC);
	
	#10$stop;
	end


endmodule
