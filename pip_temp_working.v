/*
* RISC_V_PIPELINE_CPU.v
*/


module RISC_V_PIPELINE_CPU(
input clock,
output[31:0] result

);
/*__________________________________________________________________________________*/
/*______________________________________ Wires _____________________________________*/
/*__________________________________________________________________________________*/


	/*Fetch registers and wires*/ 
		reg[31:0] to_PC = 0;
		reg reset;
		wire[31:0] incr_PC, instruction, from_PC;
		wire[6:0] IF_opcode;
		wire[4:0] IF_rs1, IF_rs2;
				


	/*Control registers and wires*/
		//wire[6:0] control_opcode = instruction[6:0];
		//wire[6:0] func7 = instruction[31:25];
		/*outtput*/
		wire[1:0] PC_SelectF; 
		wire regWriteF;
		wire[1:0] regDestF;
		wire[2:0] ALU_a_MUXF;
		wire[1:0] ALU_b_MUXF;
		wire[1:0] ALU_opF;
		wire memWriteF, memReadF;
		wire[1:0] writeback_selectF;
		wire memMUXF, memWriteMUXF;


	/*Decode registers and wires*/
		wire[31:0] writeData;
		wire[31:0] extended2, immediate_value, branch, jump_PC, outRegA, outRegB, from_zero, from_zero_large;
		wire[31:0] curr_PC;
		wire[2:0]  funct3;
		wire[6:0]  funct7;


	/*Execute registers and wires*/
		wire[31:0] ALU_Result, operandA;
		wire[31:0] branch_PC;


	/*Memory registers and Wires*/
	    wire[31:0] address;
		wire[31:0] readData, memWriteData;
		

	/*Write Back registers and wires */
		wire[31:0] new_PC;
		wire[4:0] rd;
		wire WB_regWriteF;

	/* Hazard Control Wires */
		wire HC_regWriteF;
		wire stall;
	



/*-----------------------------------------------------------------------------------------------*/
/*______________________________________ Pipeline Registers _____________________________________*/
/*-----------------------------------------------------------------------------------------------*/



	/*IF/ID Pipeline registers*/
	reg[31:0] IFID_instruction;
	reg[31:0] IFID_from_PC;
	reg[31:0] IFID_incr_PC = 0;
	reg[4:0]  IFID_writeReg; 
	reg[1:0]  IFID_regDestF; /* Isnt used anywhere, figure out its purpose again*/
	reg[6:0]  IFID_opcode;
	reg[4:0]  IFID_rs1, IFID_rs2;
	reg		  IFID_regWriteF;


	/*Wires*/
	wire[31:0] IF_incr_PC = IFID_incr_PC;
	wire[4:0]  IF_writeReg = IFID_writeReg; 

	/*ID/EX pipeline regiesters*/
	reg[31:0] IDEX_curr_PC;
	reg[31:0] IDEX_extended2;
	reg[31:0] IDEX_immediate_value;
	reg[31:0] IDEX_branch;
	reg[31:0] IDEX_outRegA, IDEX_outRegB;
	reg[31:0] IDEX_jump_PC;
	reg[31:0] IDEX_from_zero_large;
	reg[31:0] IDEX_incr_PC;
	reg[2:0]  IDEX_funct3;
	reg[6:0]  IDEX_funct7;
	reg[4:0]  IDEX_writeReg;
	//flags
	reg[1:0]  IDEX_PC_SelectF;
	reg 	  IDEX_regWriteF, IDEX_memMUXF, IDEX_memWriteMUXF;
	reg[1:0]  IDEX_regDestF;
	reg[2:0]  IDEX_ALU_a_MUXF;
	reg[1:0]  IDEX_ALU_b_MUXF;
	reg[1:0]  IDEX_ALU_opF;
	reg       IDEX_memWriteF;
	reg       IDEX_memReadF; 
	reg[1:0]  IDEX_writeback_selectF;


	

	/*wires between EX and MEM*/
	wire[31:0] ID_incr_PC		  = IDEX_incr_PC;
   	wire[31:0] ID_extended2 	  = IDEX_extended2;
	wire[31:0] ID_immediate_value = IDEX_immediate_value;
	wire[4:0]  ID_writeReg  	  = IDEX_writeReg;
	wire 	   ID_memMUXF         = IDEX_memMUXF;
	wire	   ID_memReadF 		  = IDEX_memReadF;
	wire	   ID_regWriteF		  = IDEX_regWriteF;
	wire	   ID_writeback_selectF = IDEX_writeback_selectF;
	wire 	   ID_memWriteF		    = IDEX_memWriteF;
  	/*EX/MEM pipeline regiesters*/
	reg[31:0] EXMEM_incr_PC = 0;
   	reg[31:0] EXMEM_memWriteData;
   	reg[31:0] EXMEM_ALU_Result;
   	reg[31:0] EXMEM_extended2;
   	reg[31:0] EXMEM_immediate_value;
	reg[4:0]  EXMEM_writeReg;
  	reg		  EXMEM_memMUXF;
   	reg		  EXMEM_memReadF;
	reg 	  EXMEM_regWriteF;
	reg[1:0]  EXMEM_writeback_selectF;
	reg 	  EXMEM_memWriteF;





	/*wires for Mem to Wb registers*/
	wire[31:0] MEM_ALU_Result = EXMEM_ALU_Result;
	wire[31:0] MEM_incr_PC    = EXMEM_incr_PC;
	wire 	   MEM_regWriteF  = EXMEM_regWriteF;
	wire[4:0]  MEM_writeReg   = EXMEM_writeReg;
	wire[1:0]  MEM_writeback_selectF = EXMEM_writeback_selectF;
	/*MEM to WB Registers*/	
	reg[31:0] MEMWB_ALU_Result;
	reg[31:0] MEMWB_readData;
	reg[31:0] MEMWB_incr_PC;
	reg[31:0] MEMWB_writeData;
	reg 	  MEMWB_regWriteF;
	reg[4:0]  MEMWB_writeReg;
	reg[4:0]  MEMWB_rd;
	reg[1:0]  MEMWB_writeback_selectF;
	


	always@(posedge clock) begin
			
		/*Fetch-Decode registers */
		IFID_opcode			   <= IF_opcode;
		IFID_rs1 			   <= IF_rs1;
		IFID_rs2 			   <= IF_rs2;
		IFID_instruction 	   <= instruction;
		IFID_from_PC 		   <= from_PC;
		/*Data hazard*/
		if(stall) begin
			to_PC		       <= from_PC ;
		end else begin
			to_PC 			   <= from_PC + 4;
		end
		IFID_writeReg	       <= instruction[11:7];

		/*Flags*/
		IDEX_PC_SelectF        <= PC_SelectF;
		IDEX_regWriteF		   <= regWriteF;
		IDEX_memMUXF		   <= memMUXF;
		IDEX_memWriteMUXF      <= memWriteMUXF;
		IDEX_regDestF		   <= regDestF;
		IDEX_ALU_a_MUXF 	   <= ALU_a_MUXF;
		IDEX_ALU_b_MUXF 	   <= ALU_b_MUXF;
		IDEX_ALU_opF		   <= ALU_opF;
		IDEX_memWriteF         <= memWriteF;
		IDEX_memReadF		   <= memReadF;
		IDEX_writeback_selectF <= writeback_selectF;

		/*Decode-Execute Registers*/
		IDEX_curr_PC 		   <= curr_PC;
		IDEX_extended2 		   <= extended2;
		IDEX_immediate_value   <= immediate_value;
		IDEX_branch            <= branch;
		IDEX_outRegA 		   <= outRegA;
		IDEX_outRegB 		   <= outRegB;
		IDEX_jump_PC		   <= jump_PC;
		IDEX_from_zero_large   <= from_zero_large;
		IDEX_incr_PC           <= curr_PC + 4;
		IDEX_funct3            <= funct3;
		IDEX_funct7			   <= funct7;
		IDEX_incr_PC 		   <= IF_incr_PC;
		IDEX_writeReg		   <= IF_writeReg;

		/*Execute-Memory Registers*/
		EXMEM_memWriteData     <= memWriteData;
		EXMEM_ALU_Result       <= ALU_Result;
   		EXMEM_extended2  	   <= ID_extended2; 
   		EXMEM_immediate_value  <= ID_immediate_value;
   		EXMEM_memMUXF		   <= ID_memMUXF;
   		EXMEM_memReadF		   <= IDEX_memReadF;
		EXMEM_incr_PC          <= ID_incr_PC;
		EXMEM_regWriteF 	   <= ID_regWriteF;
		EXMEM_writeReg		   <= ID_writeReg;
		EXMEM_writeback_selectF<= ID_writeback_selectF;
		EXMEM_memWriteF        <= ID_memWriteF;

		/*Memory-WriteBack Registers*/
		MEMWB_writeback_selectF<= MEM_writeback_selectF;
		MEMWB_ALU_Result 	   <= MEM_ALU_Result;
		MEMWB_readData  	   <= readData; 
		MEMWB_incr_PC		   <= MEM_incr_PC;
		MEMWB_regWriteF		   <= MEM_regWriteF;
		MEMWB_writeReg		   <= MEM_writeReg;
		MEMWB_rd			   <= rd;
		MEMWB_writeData   	   <= writeData;





		$display("\nIF:  curr_PC(%d) | instuction(%b)", uut.IFID_from_PC, uut.instruction);
		$display("ID:  rs1(%d) | rs2(%d) | write_data(%d) | writeF(%b) | writeReg(%d) | regWF(%b) | write_reg(%d)", uut.IDEX_outRegA, uut.IDEX_outRegB, uut.ID_Control1.Register_File1.writeData, uut.ID_Control1.Register_File1.regWriteF, uut.ID_Control1.Register_File1.writeReg, uut.regWriteF, uut.IFID_writeReg);
		$display("EX:  AMUX(%b) | rs1(%d) | rs(%d) = ALU_Result(%d) |immediate value(%d) | RegWF(%b) | writeReg(%d)",uut.IDEX_ALU_a_MUXF, uut.EX_Control1.operand_A_to_ALU, uut.EX_Control1.operand_B_to_ALU, uut.ALU_Result, uut.IDEX_immediate_value, uut.IDEX_regWriteF, uut.IDEX_writeReg);
		
		$display("MEM:  writeData(%d) | address(%d) | mem_writeF(%b) | regWF(%b) | writeReg(%d)", uut.EXMEM_memWriteData, uut.MEM_Control1.address, uut.memWriteF, uut.EXMEM_regWriteF, uut.EXMEM_writeReg);
		$display("addr(%d) | data(%d) | memwriteF(%b), writeReg(%d)", uut.MEM_Control1.Data_Memory1.address, uut.MEM_Control1.Data_Memory1.writeData, uut.MEM_Control1.Data_Memory1.memWriteF, uut.EXMEM_writeReg);
		$display("WB:  WF(%b) | writeReg(%d) | writeData(%d)", uut.MEMWB_regWriteF, uut.MEMWB_rd, uut.MEMWB_writeData);


		/*Write Back*/

		/*ModleSim testing*/
		/*$display("\n IFID_incr_PC(%d) | IDEX_incr_PC(%d) | EXMEM_incr_PC(%d) | MEMWB_incr_PC(%d) \n", IFID_incr_PC, IDEX_incr_PC, EXMEM_incr_PC, MEMWB_incr_PC);
		$display("\nIF:  curr_PC(%d) | instuction(%b)", uut.IFID_from_PC, uut.instruction);
		$display("ID:  rs1(%d) | rs2(%d)", uut.outRegA, uut.outRegB);
		$display("EX:  rs1(%d) | rs(%d) = ALU_Result(%d)",uut.IDEX_outRegA, uut.IDEX_outRegB, uut.EXMEM_ALU_Result);
		$display("immediate value(%d)", uut.IDEX_immediate_value);
		
		$display("");
		$display(""); 
	   $display("\nIDEX_extended2(%d) | EXMEM_extended2(%d)", IDEX_extended2, EXMEM_extended2);
	   $display("IDEX_immediate_value(%d) | EXMEM_immediate_value(%d)\n", IDEX_immediate_value, EXMEM_immediate_value);  */
	end	



/*--------------------------------------------------------------------------------------------------------------*/
/*______________________________________________ Wiring Processor ______________________________________________*/
/*--------------------------------------------------------------------------------------------------------------*/

	IF_Control IF_Control1(
		//Inputs
		.to_PC(to_PC), 	 /*Try just incr_PC later */
		.PC_SelectF(PC_SelectF),
		.branch_PC(IDEX_branch), /*Need to calc this in IF stage */
		.jump(IDEX_jump_PC),     /*Need to calc in IF stage      */
		.JALR_Result(ALU_Result),/*????? probably need NOP's     */
		.clock(clock),
		.reset(reset),
		//output
//		.incr_PC(incr_PC),
		.instruction(instruction),
		.from_PC(from_PC),
		.rs1(IF_rs1),
		.rs2(IF_rs2),
		.opcode(IF_opcode)
	);

	/*What to do for pipeline reg for flags?
	* Keep Assign them all to IFID regs and pass them all along and drop off
	* once used?
	*/


  
	control_unit CU1(
		/*input*/
		.control_opcode(IFID_instruction[6:0]),
		.func7(IFID_instruction[31:25]),
		/*output*/
		.memWriteMUXF(memWriteMUXF),
		.PC_SelectF(PC_SelectF),
		.memMUXF(memMUXF),
		.regWriteF(regWriteF),
		.regDestF(regDestF),
		.ALU_a_MUXF(ALU_a_MUXF),
		.ALU_b_MUXF(ALU_b_MUXF),
		.ALU_opF(ALU_opF),
		.memWriteF(memWriteF),
		.memReadF(memReadF),
		.writeback_selectF(writeback_selectF)
	);




	ID_Control ID_Control1(
		//input
		.instruction(IFID_instruction),
		.from_PC(IFID_from_PC),
		.rd(MEMWB_rd), /*From Write back, no pipeline reg yet*/
		.writeData(MEMWB_writeData), /*from write back, no pipeline reg yet*/
		.regWriteF(MEMWB_regWriteF), /*From write back, no pipeline reg yet*/ 
		.clock(clock),
		.regDestF(regDestF),
		//output
		.extended2(extended2),
		.immediate_value(immediate_value),
		.branch(branch),
		.jump_PC(jump_PC),
		.outRegA(outRegA),
		.outRegB(outRegB),
		.from_zero(from_zero),
		.from_zero_large(from_zero_large),
		.PC(curr_PC),
		.funct3(funct3),
		.funct7(funct7)
	);

	hazard_control HC(
		.curr_rs1(IFID_rs1),
		.curr_rs2(IFID_rs2),
		.opcode(IFID_opcode),
		.EX_rd(IDEX_writeReg),
		.MEM_rd(EXMEM_writeReg),
		.inRegWriteF(EXMEM_regWriteF),
		.regWriteF(HC_regWriteF),
		.stall(stall)	
	);


	EX_Control EX_Control1(
		//input
		.PC(IDEX_curr_PC),
		.extended2(IDEX_extended2),
		.immediate_value(IDEX_immediate_value),
		.branch(IDEX_branch),
		.incr_PC(IDEX_incr_PC), /*From IF, not done yet */
		.jump_PC(jump_PC), /*Can delete this, dont think it is conencted up to anything. */
		.outRegA(IDEX_outRegA),
		.outRegB(IDEX_outRegB),
		.from_zero(IDEX_jump_PC),/* This is the jump pc, rename this */
		.from_zero_large(IDEX_from_zero_large),
		.ALU_a_MUXF(IDEX_ALU_a_MUXF), /*FROM CU, not done yet*/
		.ALU_opF(IDEX_ALU_opF), /* From CU, not done yet*/
		.funct3(IDEX_funct3), /*From fetch, may need to pass whole instruction through ID or jsut this specific value. though ID.*/
		.funct7(IDEX_funct7), /*From fetch or need to pass instrucion or specific index from instruction through ID.*/ 
		.ALU_b_MUXF(IDEX_ALU_b_MUXF), /* FROM CU, not done yet*/


		//output
		.zeroF(zeroF),
		.ALU_Result(ALU_Result),
		.operandA(operandA),
		.branch_PC(branch_PC)
	);
	

	/*
	mux2to1 memMux(
		.gen0(extended2),
		.gen1(immediate_value),
		.sel(memMUXF),
		.gen_out(address)
	); */

	assign memWriteData = (memWriteMUXF) ? ALU_Result : outRegB;
	assign address      = (IDEX_memMUXF) ? IDEX_immediate_value : IDEX_extended2;
	/*
	mux2to1 memWriteDataMux(
		.gen0(ALU_Result),
		.gen1(outRegB),
		.sel(memWriteMUXF),
		.gen_out(memWriteData)
	);*/




	MEM_Control MEM_Control1(
		.address(address),
		.writeData(EXMEM_memWriteData), /*writing rs2(src) to memory*/
		.extended2(EXMEM_extended2),
		.immediate_value(EXMEM_immediate_value),
		.memMUXF(EXMEM_memMUXF),
		.memWriteF(EXMEM_memWriteF),
		.clock(clock),
		.memReadF(EXMEM_memReadF),
		//output
		.readData(readData)
	);

	
	
	WB_Control WB_Control1(
		//input
		.ALU_Result(MEMWB_ALU_Result),
		.readData(MEMWB_readData),
		.incr_PC(MEMWB_incr_PC),
		.regWriteF(MEMWB_regWriteF),
		.writeReg(MEMWB_writeReg),
//		.branch_PC(branch_PC),
//		.jump(jump_PC),
		.memToRegF(MEMWB_writeback_selectF),
//		.curr_PC(to_PC),
//		.PC_SelectF(PC_SelectF),
//		.to_PC(new_PC),


		//output
		.writeData(writeData),
		.WB_regWriteF(WB_regWriteF),
		.rd(rd)
	);
	

endmodule
