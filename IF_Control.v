module IF_Control(
	input[31:0]to_PC, branch_PC, JALR_Result,
	input clock, reset,
	input[1:0] PC_SelectF,
	//output[31:0]incr_PC,
	output[31:0]instruction, jump_PC, incr_PC,
	output[31:0]from_PC,
	output[6:0] opcode,
	output[4:0] rs1, rs2
);
wire[31:0] PC;
wire[31:0] jump;
wire[31:0] branch;



	


	Instruction_Memory Instruction_Memory1(
		.from_PC(to_PC),
		.clock(clock),
		.reset(reset),
		.instruction(instruction)
	);

	wire[11:0] B_immediate = {instruction[31], instruction[7], instruction[30:25], instruction[11:8]};
	branch_target branch_target1(
		.from_PC(to_PC),
		.B_immediate(B_immediate),
		.branch(branch)
	);


	wire[19:0] jump_offest = {instruction[31], instruction[19:12], instruction[20], instruction[30:21]};
	
	/*
	* This handles the jump instructions from the risc-v V1 ISA not the JAL
	* and JALR instructions from the risc-v v2 isa.
	*/
	jump_target jump_target1(
		.from_PC(to_PC),
		.jump_offest(jump_offest),	
		.jump_PC(jump)
	);

	mux4to1 PC_Selector(
		.gen0(to_PC + 4),		/*PC_SelectF = 2'b00*/
		.gen1(branch),   /*PC_SelectF = 2'b01*/
		.gen2(jump),		/*PC_SelectF = 2'b10*/
		.gen3(JALR_Result),	/*PC_SelectF = 2'b11*/
		.sel(PC_SelectF), 
		.gen_out(PC)
	);

	ProgramCounter ProgramCounter1(
		.to_PC(PC),
		.reset(reset),
		.clock(clock),
		.from_PC(from_PC)
	);

	//assign PC_write = 1;
	assign incr_PC = to_PC + 4;
	// PC_Adder PC_Adder1(
	// 	.from_PC(from_PC),
	// 	.incr_PC(incr_PC)
	// );
	assign jump_PC = jump;
	assign rs1    = instruction[19:15];
	assign rs2    = instruction[24:20];
	assign opcode = instruction[6:0];
endmodule
