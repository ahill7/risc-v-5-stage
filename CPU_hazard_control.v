/*
* hazard_control_tb.v
*/

module CPU_hazard_control(input clock);

	reg[31:0] PC = 0;
	
	wire prev_PC = PC;

	reg[4:0] curr_rs1, curr_rs2, EX_rd, MEM_rd;
	reg[6:0] opcode;
	reg reg_writeF;
	wire regWriteF, stall_cycles;

	always@(*)begin
			reg_writeF <= regWriteF;
	end

	always@(posedge clock) begin
		if(!stall_cycles)begin
			PC <= PC + 4;
		end

	end



	hazard_control HC(
		.curr_rs1(curr_rs1),
		.inRegWriteF(reg_writeF),
		.opcode(opcode),
		.curr_rs2(curr_rs2),
		.EX_rd(EX_rd),
		.MEM_rd(MEM_rd),
		.regWriteF(regWriteF),
		.stall(stall_cycles)
	);



endmodule
