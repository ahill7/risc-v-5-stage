/*
* zero_extend_TB.v
*/


module zero_extend_TB;
	reg[4:0] to_zero;
	wire[31:0] from_zero;
	integer i;

	zero_extend uut(
		.to_zero(to_zero),
		.from_zero(from_zero)
	);


	initial begin
		$display("\n#########################################################################");
		$display("\t\t\t zero extend test bed");
		$display("#########################################################################\n");
		

		for(i=0; i<10; i = i+1) begin
			to_zero=$random;
			#10
			$display("to_zero(%b) || from_zero(%b)", to_zero, from_zero);
		end

	end
endmodule
