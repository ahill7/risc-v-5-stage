/* 
* sing_extend_TB.v
*
*/


module sing_extend_TB;
	reg[11:0] to_extend;
	wire[31:0] from_extend;
	integer i;

	sign_extend uut(
		.to_extend(to_extend),
		.from_extend(from_extend)
	);


	initial begin

		$display("\n###############################################################################");
		$display("\t\t\t\tsign extend test bed");
		$display("###############################################################################\n");

		to_extend=1;
		#10;
		$display("to_extend(%b) || from_extend(%b)",to_extend, from_extend);
		for(i=0; i<20; i=i+1) begin
			to_extend=i;
			#10;
			$display("to_extend(%b) || from_extend(%b)",to_extend, from_extend);

		end

		to_extend =  12'b10_00_00_11_00_11;
		#10;
		$display("to_extend(%b) || from_extend(%b)",to_extend, from_extend);


	end


endmodule
