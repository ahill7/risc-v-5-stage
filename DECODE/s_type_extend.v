/*
* s_type_sign_extend.v
* Andrew Hill
*/


module s_type_sign_extend(
	input[6:0] most_sig,
	input[4:0] least_sig,
	output[31:0] extended2
);
	reg[11:0] third_source; 
	always@(*) begin
	third_source = {most_sig, least_sig};
	end

	assign extended2 = {{20{third_source[11]}}, third_source[11:0]};

endmodule

