/*
 * Andrew Hill
 * CAES LAB
 * Univeristy of Oregon
 * June 2016
 *
 * Regiester file consiting of 32, 32 bit registers.
 *
 */

module Register_File(
    input regWriteF, clock,
    input[4:0] inRegA, inRegB, writeReg,
	input[31:0] writeData,
    output reg[31:0] outRegA, outRegB

);

    reg[31:0] reg_file [0:31];

    always@(*) begin
       // if(regWriteF) $display("\n\n\n############# writing(%d) #######################", writeData);
//        reg_file[writeReg] = regWriteF ? writeData : reg_file[writeReg];

        if(regWriteF && writeReg != 0)begin
            reg_file[writeReg] <= writeData;
        end 
     //  else reg_file[writeReg] <= reg_file[writeReg];

    end

    always@(*)begin
    	if(inRegA == 0) outRegA = 32'b0;
    	else outRegA = reg_file[inRegA];
    	if(inRegB == 5'b0) outRegB = 32'b0;
    	else outRegB = reg_file[inRegB];
    end


endmodule
