/* 
* zero_extend.v
* Andrew Hill
* 
* Not sure if this is the correct size of the instruction that will be
* extended. This module works, just need to make sure input port is right size
* and figure out which segmant of the instruction will get sent to it.
*/


module zero_extend(
	input[4:0] to_zero,
	output[31:0] from_zero 
);

	assign from_zero = {27'b0,to_zero};

endmodule
