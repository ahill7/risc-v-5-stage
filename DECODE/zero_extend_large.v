/* 
* zero_extend_large.v
* Andrew Hill
*
*
*
*
*/



module zero_extend_large(
	input[19:0] to_zero_large,/*instruction[31:12] LUI(Load Upper Immediate), AUIPC(Add Upper Immediate to PC)*/
	output[31:0] from_zero_large
);

	assign from_zero_large = {to_zero_large,12'b0};
	




endmodule
