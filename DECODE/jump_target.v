/* 
* jump_target.v
*
* Andrew Hill
*
* J-Type: 25-bit jump target address as a PC-relative offsete. The 25-bit
* immediate value is shifted left by one and added to the current PC(PC before
* being incremented by 4) to form the target address.
*/


module jump_target(
	input[31:0] from_PC,
	input signed[19:0] jump_offest,
	output[31:0] jump_PC
);

	wire signed [20:0] shifted_offset = jump_offest << 1; 
	always@(shifted_offset)begin
		$display("\n\n\n jump shifted_offset(%d)", shifted_offset);
	end
	assign jump_PC = {{11{shifted_offset[20]}}, shifted_offset[20:0]} + from_PC;

endmodule
