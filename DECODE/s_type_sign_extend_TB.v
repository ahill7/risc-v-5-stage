/* 
* b_type_sign_extend_TB
*/


module b_type_sign_extend_TB;
	reg[4:0] most_sig;
	reg[6:0] least_sig;
	wire[31:0] extended;
	integer i;

	b_type_sign_extend uut(
		.most_sig(most_sig),
		.least_sig(least_sig),
		.extended(extended)
	);


	initial begin
		$display("\n##################################################################################");
		$display("\t\t\t\tB-Type sign extendtion");
		$display("##################################################################################\n");

		most_sig=5'b10_00_1;
		least_sig=7'b11_00_00_0;
		#10;
		$display("MS(%b), LS(%b), CC(%b), extended(%b)", most_sig, least_sig, uut.third_source, extended);
	//test 2	
		most_sig=0;
		least_sig=7'b11_00_00_0;
		#10;
		$display("MS(%b), LS(%b), CC(%b), extended(%b)", most_sig, least_sig, uut.third_source, extended);
	//Test 3	
		most_sig=5'b10_00_1;
		least_sig=0;
		#10;
		$display("MS(%b), LS(%b), CC(%b), extended(%b)", most_sig, least_sig, uut.third_source, extended);
	
		for(i=0; i<20; i = i+1) begin
			most_sig=$random;
			least_sig=$random;
			#10;
			$display("MS(%b), LS(%b), CC(%b), extended(%b)", most_sig, least_sig, uut.third_source, extended);
	
		end

	end
endmodule
