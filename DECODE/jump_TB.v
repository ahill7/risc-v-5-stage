/* 
* jump_TB.v
*
* Andrew Hill
*
*/


module jump_TB;
	reg[31:0] incrPC;
	reg[19:0] jump_offest;
	wire[31:0] jump_PC;
	integer i;


	jump_target uut(
		.from_PC(incrPC),
		.jump_offest(jump_offest),
		.jump_PC(jump_PC)
	);
	
	initial begin
		$display("\n#############################################################################");
		$display("\t\t\t\t jump test bed");
		$display("#############################################################################\n");

		incrPC = 0;
		jump_offest = 2;
		#10;
		$display("incrPC(%d), jump_offest(%d), jump_PC(%d)", incrPC, jump_offest[1:0], jump_PC);
		for(i=0; i < 20; i = i +1) begin
			incrPC = jump_PC;
			#10;
			$display("incrPC(%d), jump_offest(%d), jump_PC(%d)", incrPC, jump_offest[1:0], jump_PC);
		end
	end

endmodule
