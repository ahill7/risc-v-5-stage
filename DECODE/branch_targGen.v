/* 
* branch_targGen.v 
*
* Andrew Hill
* CAES Lab, Univerity of Oregon
* June 2016
*
*/


module branch_target(
	input[31:0] from_PC, 
	input[11:0] B_immediate,
	output[31:0] branch 
);

	wire signed[12:0] branch_value = B_immediate << 1;
	wire signed[31:0] branch_PC = {{19{branch_value[12]}}, branch_value[12:0]};
	assign branch = branch_PC + from_PC; 
endmodule
