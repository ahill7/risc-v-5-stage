/* 
* sign_extend.v
* Andrew Hill
*/


module sign_extend(
	input[11:0] to_extend, /*instruction[31:20]*/
	output[31:0] immediate_value
);

		assign immediate_value = {{20{to_extend[11]}}, to_extend[11:0] };
	

endmodule
