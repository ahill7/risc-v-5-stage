/*
* RISC_V_PIPELINE_CPU_TB.v
*/

module RISC_V_PIPELINE_CPU_TB;
	reg clock;
	wire[31:0] result;
	integer i=0, num_instructions=29, j=0;

	RISC_V_PIPELINE_CPU uut(
		.clock(clock),
		.result(result)
	);





	always begin
		#5 clock = !clock;
	//	uut.to_PC = uut.new_PC;
	end
	initial begin
		//uut.reset = 1;
		uut.state  <= 2'b10; /*Starts in INIT state.*/
		#1;
		//uut.reset = 0;
		clock = 0;
	end




	initial begin
		$display("\n#################################################################################");
		$display("\t\t\t RISC-V TEST BED");
		$display("#################################################################################\n");

        $monitor("reg[3] = (%d)", uut.ID_Control1.Register_File1.reg_file[3] );

        #10;

		 $display("\n######################## Register Values ################################");

		for(i=0; i<32; i=i+1) begin
		 	$display("reg_file[%d] = %d", i[5:0], uut.ID_Control1.Register_File1.reg_file[i]);
		end
	

        //while(uut.to_PC < 132) begin
        while(uut.to_PC < 56)begin
            #10;
        end
        #50
        

		 $display("\n######################## Register Values ################################");

		for(i=0; i<32; i=i+1) begin
		 	$display("reg_file[%d] = %d", i[5:0], uut.ID_Control1.Register_File1.reg_file[i]);
		end
		
		
		$display("\n\ndata_mem[4]  = %d", uut.MEM_Control1.Data_Memory1.DATA_RAM[4]);

	    $stop;

	end


endmodule

