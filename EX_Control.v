/*
* EX_Control.v 
* Andrew Hill 
*
* 
*
* TODO: Need to make sure operandA is being passed along. 
*/

module EX_Control(
	input[31:0] extended2, immediate_value, branch, outRegA, outRegB, from_zero, from_zero_large, PC, incr_PC,
	input[2:0] ALU_a_MUXF, 
	input[1:0] ALU_opF, ALU_b_MUXF,
	input[2:0] funct3,
	input[6:0] funct7,
	output zeroF,
	output[31:0] ALU_Result, operandA, branch_PC
	);


	wire [2:0] ALUControlOpcode;
	wire[31:0] operand_A_to_ALU, operand_B_to_ALU;
	wire[6:0] funct7_out;

	ALU_Control ALU_Controler1(
		.funct3(funct3),
		.funct7_in(funct7),
		.ALU_opF(ALU_opF),
		.ALUControlOpcode(ALUControlOpcode),
		.funct7_out(funct7_out)
	);


	mux5to1 operand_a(
		.gen1(outRegA),
		.gen2(immediate_value),
		.gen3(from_zero),
		.gen4(from_zero_large),
		.gen5(extended2),
		.sel(ALU_a_MUXF),
		.gen_out(operand_A_to_ALU)
	);

	mux3to1 operand_b(
		.gen0(outRegB),
		.gen1(PC),
		.gen2(0),
		.sel(ALU_b_MUXF),
		.gen_out(operand_B_to_ALU)
	);


	ALU ALU1(
		.rs2(operand_A_to_ALU),
		.rs1(operand_B_to_ALU),
		.branch(branch),
		.incr_PC(incr_PC),
		.funct3(funct3),
		.ALUControlOpcode(ALUControlOpcode),
		.funct7(funct7_out),
		.ALUResult(ALU_Result),
		.zeroF(zeroF),
		.branch_PC(branch_PC)
	);	



endmodule