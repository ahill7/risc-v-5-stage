/*
*ID_Control.v
*/


module ID_Control(
	input[31:0] instruction,
	input[4:0] rd,
	input[31:0] from_PC,
	input[31:0] writeData,
	input regWriteF, clock,
	input[1:0] regDestF,
	output[2:0] funct3,
	output[6:0] funct7,
	output[31:0] extended2, immediate_value, branch, outRegA, outRegB, from_zero, from_zero_large, PC

);


	/* Proper indexing for R-Type(regiester-register) operation	*/

	//wire[6:0] fucnt7 = instruction[31:25];
	wire[4:0] rs2 	 = instruction[24:20];
	wire[4:0] rs1	 = instruction[19:15];
	//wire[2:0] funct3 = instruction[14:12];
	//wire[4:0] rd 	 = instruction[11:7];
	wire[6:0] opCode = instruction[6:0];

	/*
	* Need to check both these the make sure use proper index of instruction
	* for the risc-v v2 isa. 
	*/
	wire[6:0] most_sig = instruction[31:25];
	wire[4:0] least_sig = instruction[11:7];

	s_type_sign_extend s_type1(
		.most_sig(most_sig),
		.least_sig(least_sig),
		.extended2(extended2)
	);

	wire[11:0] to_extend = instruction[31:20];

	sign_extend sign_extend1(
		.to_extend(to_extend),
		.immediate_value(immediate_value)
	);

	// wire[11:0] B_immediate = {instruction[31], instruction[7], instruction[30:25], instruction[11:8]};
	// branch_target branch_target1(
	// 	.from_PC(from_PC),
	// 	.B_immediate(B_immediate),
	// 	.branch(branch)
	// );

	// wire[19:0] jump_offest = {instruction[31], instruction[19:12], instruction[20], instruction[30:21]};
	
	
	// * This handles the jump instructions from the risc-v V1 ISA not the JAL
	// * and JALR instructions from the risc-v v2 isa.
	
	// jump_target jump_target1(
	// 	.from_PC(from_PC),
	// 	.jump_offest(jump_offest),	
	// 	.jump_PC(jump_PC)
	// );	

	/* These are rs1, rs2 and rd above, respectivly
	reg inRegA = instruction[];
	reg inRegB = instruction[];
	reg writeReg = instruction[];
	*/


	Register_File Register_File1(
		.regWriteF(regWriteF),
		.clock(clock),
		.inRegA(rs2),
		.inRegB(rs1),
		.writeReg(rd),
		.writeData(writeData),
		.outRegA(outRegA),
		.outRegB(outRegB)
	);


	wire[4:0] to_zero = instruction[14:10];

	zero_extend zero_extend1(
		.to_zero(to_zero),
		.from_zero(from_zero)
	);	

	wire[19:0] to_zero_large = instruction[31:12];

	zero_extend_large zero_extend_large1(
		.to_zero_large(to_zero_large),
		.from_zero_large(from_zero_large)
	);

assign PC = from_PC;
// assign doenregWriteF=0;
assign funct7 =  instruction[31:25];
assign funct3 = instruction[14:12];



endmodule
